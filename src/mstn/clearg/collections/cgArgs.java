package mstn.clearg.collections;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.annotations.JSFunction;

public class cgArgs extends ScriptableObject {

	private ArrayList<cgArgument> list;
	private static final long serialVersionUID = 1L;
	
	ArrayList<cgArgument> test;
	
	public cgArgs(){
		list = new ArrayList<cgArgument>();
	}
	
	@Override
    public Object get(String name, Scriptable start) {

		if (name.equals("add")) {
			return new Function(){
				
				@Override
				public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
					String value;
					if (args[0] instanceof NativeJavaObject){
						value = (String)((NativeJavaObject)args[0]).unwrap();
					} else {
						value = (String)args[0];
					} 
					int type;
					if (args[1] instanceof NativeJavaObject){
						type = (Integer)((NativeJavaObject)args[1]).unwrap();
					} else if (args[1] instanceof Double){
						type = ((Double)args[1]).intValue();
					} else {
						type = (Integer)args[1];
					}
					
					return add(value, type);
				}
				
				@Override
				public void put(int arg0, Scriptable arg1, Object arg2) {
					
				}
				
				@Override
				public void put(String arg0, Scriptable arg1, Object arg2) {
					
				}

				@Override
				public void delete(String arg0) {
					
				}

				@Override
				public void delete(int arg0) {
					
				}

				@Override
				public Object get(String name, Scriptable start) {
					return null;
					
				}

				@Override
				public Object get(int arg0, Scriptable arg1) {
					return null;
				}

				@Override
				public String getClassName() {
					return null;
				}

				@Override
				public Object getDefaultValue(Class<?> arg0) {
					
					return null;
				}

				@Override
				public Object[] getIds() {
					
					return null;
				}

				@Override
				public Scriptable getParentScope() {
					return null;
				}

				@Override
				public Scriptable getPrototype() {
					return null;
				}

				@Override
				public boolean has(String arg0, Scriptable arg1) {
					
					return false;
				}

				@Override
				public boolean has(int arg0, Scriptable arg1) {
					
					return false;
				}

				@Override
				public boolean hasInstance(Scriptable arg0) {
					
					return false;
				}

				@Override
				public void setParentScope(Scriptable arg0) {
					
					
				}

				@Override
				public void setPrototype(Scriptable arg0) {
					
					
				}

				@Override
				public Scriptable construct(Context arg0, Scriptable arg1,
						Object[] arg2) {
					
					return null;
				}
			};
        } else if (name.equals("getList")){
        	return new Function() {
				
	
        		
				@Override
				public void setPrototype(Scriptable arg0) {
					
					
				}
				
				@Override
				public void setParentScope(Scriptable arg0) {
					
					
				}
				
				@Override
				public void put(int arg0, Scriptable arg1, Object arg2) {
					
					
				}
				
				@Override
				public void put(String arg0, Scriptable arg1, Object arg2) {
					
					
				}
				
				@Override
				public boolean hasInstance(Scriptable arg0) {
					
					return false;
				}
				
				@Override
				public boolean has(int arg0, Scriptable arg1) {
					
					return false;
				}
				
				@Override
				public boolean has(String arg0, Scriptable arg1) {
					
					return false;
				}
				
				@Override
				public Scriptable getPrototype() {
					
					return null;
				}
				
				@Override
				public Scriptable getParentScope() {
					
					return null;
				}
				
				@Override
				public Object[] getIds() {
					
					return null;
				}
				
				@Override
				public Object getDefaultValue(Class<?> arg0) {
					
					return null;
				}
				
				@Override
				public String getClassName() {
					
					return null;
				}
				
				@Override
				public Object get(int arg0, Scriptable arg1) {
					
					return null;
				}
				
				@Override
				public Object get(String arg0, Scriptable arg1) {
					
					return null;
				}
				
				@Override
				public void delete(int arg0) {
					
					
				}
				
				@Override
				public void delete(String arg0) {
					
					
				}
				
				@Override
				public Scriptable construct(Context arg0, Scriptable arg1, Object[] arg2) {
					
					return null;
				}
				
				@Override
				public Object call(Context context, Scriptable scope, Scriptable thisObj,
						Object[] args) {
					return getList();
				}
			};
        } else if (name.equals("length")){
        	return new Function() {
				
				@Override
				public void setPrototype(Scriptable arg0) {
					
					
				}
				
				@Override
				public void setParentScope(Scriptable arg0) {
					
					
				}
				
				@Override
				public void put(int arg0, Scriptable arg1, Object arg2) {
					
					
				}
				
				@Override
				public void put(String arg0, Scriptable arg1, Object arg2) {
					
					
				}
				
				@Override
				public boolean hasInstance(Scriptable arg0) {
					
					return false;
				}
				
				@Override
				public boolean has(int arg0, Scriptable arg1) {
					
					return false;
				}
				
				@Override
				public boolean has(String arg0, Scriptable arg1) {
					
					return false;
				}
				
				@Override
				public Scriptable getPrototype() {
					
					return null;
				}
				
				@Override
				public Scriptable getParentScope() {
					
					return null;
				}
				
				@Override
				public Object[] getIds() {
					
					return null;
				}
				
				@Override
				public Object getDefaultValue(Class<?> arg0) {
					
					return null;
				}
				
				@Override
				public String getClassName() {
					
					return null;
				}
				
				@Override
				public Object get(int arg0, Scriptable arg1) {
					
					return null;
				}
				
				@Override
				public Object get(String arg0, Scriptable arg1) {
					
					return null;
				}
				
				@Override
				public void delete(int arg0) {
					
					
				}
				
				@Override
				public void delete(String arg0) {
					
					
				}
				
				@Override
				public Scriptable construct(Context arg0, Scriptable arg1, Object[] arg2) {
					return null;
				}
				
				@Override
				public Object call(Context arg0, Scriptable arg1, Scriptable arg2,
						Object[] arg3) {
					return length();
				}
			};
        }
        return super.get(name, start);
    }

	
	@JSFunction
	public cgArgs add(Object value, int type){
		if (value instanceof Boolean){
			if ((Boolean)value == true){
				value = 1;
			} else {
				value = 0;
			}
		}
		
		if (value instanceof NativeJavaObject){
			value = ((NativeJavaObject)value).unwrap();
		}
		
		if (type == 4 && String.valueOf(value).isEmpty() ){
			value = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		}
		
		cgArgument arg = new cgArgument();
		arg.setType(type);
		arg.setValue(String.valueOf(value));
		list.add(arg);
		return this;
	}
	
	@JSFunction
	public ArrayList<cgArgument> getList(){
		return list;
	}

	@JSFunction
	public int length(){
		return list.size();
	}

	@Override
	public String getClassName() {
		
		return "cgArgs";
	}
	
}
