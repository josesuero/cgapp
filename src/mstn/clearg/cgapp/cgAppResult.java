package mstn.clearg.cgapp;

import mstn.clearg.language.cgLanguage;

import org.json.JSONException;
import org.json.JSONObject;

public class cgAppResult extends JSONObject {
	
	public enum codeTypes{
		S200("OK", "Success!"),
		S304("Not Modified", "There was no new data to return."),
		S400("Bad Request", "The request was invalid. An accompanying error message will explain why."),
		S401("Unauthorized", "Authentication credentials were missing or incorrect."),
		S403("Forbidden", "The request is understood, but it has been refused due to permition limits."),
		S404("Not Found", "The Procedure or the resource requested does not exists."),
		S406("Not Acceptable", "Returned by the ClearG when an invalid format is specified in the request."),
		S420("Enhance Your Calm", ""),
		S500("Internal Server Error", "Script Error"),
		S502("Bad Gateway", "ClearG is down or being upgraded."),
		S503("Service Unavailable", "The ClearG servers are up, but overloaded with requests. Try again later.");
		
		private String statusText;
		private String statusInfo;
		
		codeTypes(String statusText, String statusInfo){
			this.statusText = statusText;
			this.statusInfo = statusInfo;
		}
		
		public String getStatusText(){
			return this.statusText;
		}
		
		public String getStatusInfo(){
			return this.statusInfo;
		}
		
	}
	
	public cgAppResult(String webService){
		try{
			this.put("content", "");
			this.put("status", "");
			this.put("statusText", "");
			this.put("time", (long)0);
			this.put("service", webService);
			this.put("server","");
		} catch(Exception ex){
			
		}
	}
	
	public cgAppResult(String content, int status, long time){
		try{
			this.put("content", content);
			this.setStatus(status);
			this.setTime(time);
		} catch (Exception ex){
			
		}
	}
	
	public String getContent(){
		try{
			return this.getString("content");
		} catch(Exception ex){
			return ex.getMessage();
		}
	}
	
	public String getStatus(){
		try{
			return this.getString("status");
		} catch (Exception ex){
			return ex.getMessage();
		}
	}
	
	public String getStatusText(){
		try{
			return this.getString("statusText");
		} catch (Exception ex){
			return ex.getMessage();
		}
	}

	public long getTime(){
		try {
			return this.getLong("time");
		} catch (JSONException e) {
			return 0;
		}
	}

	public void setContent(Object content){
		try{
			this.put("content", content);
		} catch (Exception ex){
			ex.getMessage();
		}
	}
	
	public void setStatus(int status){
		try{
			this.put("status", status);
			codeTypes type = codeTypes.valueOf(cgLanguage.concat("S" ,String.valueOf(status)));
			this.put("statusText", type.getStatusText());
			this.put("statusInfo", type.getStatusInfo());
		} catch (Exception ex){
			ex.getMessage();
		}
	}

	public void setTime(long time){
		try{
			this.put("time", time);
		} catch(Exception ex){
			ex.getMessage();
		}
	}
}
