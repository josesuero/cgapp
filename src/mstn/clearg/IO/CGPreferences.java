package mstn.clearg.IO;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import mstn.clearg.language.cgLanguage;

public class CGPreferences {
	
	private Preferences prefs;
		
	public CGPreferences(String schema, String user){
		this(cgLanguage.concat(schema,"/",user));
	}
	
	public CGPreferences(String node){
		prefs = Preferences.userNodeForPackage(getClass()).node(node);
	}
	
	public String get(String name){
		
		return prefs.get(name,"");
	}
	
	public void put(String name, String value){
		this.prefs.put(name,value);
	}
	
	public void remove(String name){
		prefs.remove(name);
	}
	
	public void clear() throws BackingStoreException{
		prefs.clear();
	}
	
	public void flush() throws BackingStoreException{
		prefs.flush();
	}
}
