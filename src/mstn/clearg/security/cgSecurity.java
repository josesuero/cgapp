package mstn.clearg.security;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.cgAppResult;
import mstn.clearg.language.cgLanguage;
import mstn.clearg.language.cgProcedure;
import mstn.clearg.rest.RestClient;


/*
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
*/
public class cgSecurity {

	private Client client;
	private String uri;
	private boolean enabled = false;
		
	public cgSecurity() {
		//String uri = sc.getInitParameter("ssouri");

		String uri = "";
		try {
			InitialContext ic = new InitialContext();
			uri = ic.lookup("java:global/ssouri").toString();
			String securityEnabled = ic.lookup("java:global/securityEnabled").toString();
			if (securityEnabled.equalsIgnoreCase("true")){
				enabled = true;
			}
				
		} catch (NamingException e) {
			e.printStackTrace();
		}
				
		client = ClientBuilder.newClient();
		
		//TODO FOLLOW REDIRECTS - TIME OUT
		//client.getConfiguration().getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true);
		//client.setConnectTimeout(3000);
		
		// uri = "http://localhost:8080/sso/identity";
		this.uri = uri;
	}

	private cgAppResult getResource(String uri,	JSONObject queryParams) throws NumberFormatException, cgAppException {
		JSONObject cookies = new JSONObject(); 
		return getResource(uri,queryParams,cookies);
	}

	private cgAppResult getResource(String uri, JSONObject queryParams, JSONObject cookies) throws cgAppException {
		try {
			cgAppResult result = new cgAppResult("cgSecurity");

			JSONObject requestObj = new JSONObject();
			requestObj.put("url", uri);
			requestObj.put("type", MediaType.APPLICATION_JSON);
			requestObj.put("params", queryParams);
			requestObj.put("cookies", cookies);
			requestObj.put("accept", MediaType.APPLICATION_JSON);
			requestObj.put("method", "post");

			JSONObject response = RestClient.request(client, requestObj);

			/*
			 * WebResource resource = client.resource(uri); WebResource.Builder
			 * builder =
			 * resource.queryParams(queryParams).type(MediaType.APPLICATION_JSON
			 * ); for (Cookie c : cookies){ builder = builder.cookie(c); }
			 * ClientResponse response =
			 * builder.accept(MediaType.APPLICATION_JSON).post(ClientResponse.
			 * class); result.setStatus(response.getStatus());
			 * result.setContent("");
			 */
			result.setStatus(response.getInt("status"));
			if (response.getInt("status") == 200) {
				Object content = response.getString("content").replace("\r", "").replace("\n", "");
				result.setContent(content);
				
			} else {
				throw new cgAppException(Integer.valueOf(result.getStatus()),
						response.getString("content").replace("\r", "").replace("\n", ""));
			}
			// TODO: Report Error
			return result;
		} catch (cgAppException e) {
			throw e;
		} catch (Exception e) {
			throw new cgAppException(500, cgLanguage.concat(e.toString(), " URL: ", uri));
		}

	}

	
	public boolean isTokenValid(String token) throws NumberFormatException,
			cgAppException, JSONException {
		
		if (!enabled){
			return true;
		}
		
		JSONObject queryParams = new JSONObject(); 
		queryParams.put("tokenid", token);
		

		try {
			String content = getResource(
					cgLanguage.concat(uri, "/isTokenValid"), queryParams)
					.getContent();
			if (new JSONObject(content).getBoolean("result")) {
				return true;
			}

			return false;
		} catch (cgAppException e) {
			if (e.getStatus() == 401){
				return false;
			} else {
				throw e;
			}
		}
	}

	public cgAppResult login(String username, String password, String schema, String token)
			throws NumberFormatException, cgAppException, JSONException {
		JSONObject queryParams = new JSONObject();
		
		if (!enabled){
			cgAppResult result = new cgAppResult("cgSecurity");
			result.setStatus(200);
			result.setContent("token");
			return result;
		}
		
		queryParams.put("username", username);
		queryParams.put("password", password);
		queryParams.put("schema", schema);
		
		JSONObject cookies = new JSONObject();
		if (token != null && !token.isEmpty())
			cookies.put("JSESSIONID", token);
		
		return getResource(cgLanguage.concat(this.uri, "/authenticate"), queryParams,cookies);
	}

	public cgAppResult logout(String token) throws NumberFormatException,
			cgAppException, JSONException {
		if (!enabled){
			cgAppResult result = new cgAppResult("cgSecurity");
			result.setStatus(200);
			result.setContent("token");
			return result;
		}
		if (isTokenValid(token)) {
			JSONObject queryParams = new JSONObject();
			queryParams.put("subjectid", token);
			return getResource(cgLanguage.concat(uri, "/logout"), queryParams);
		} else {
			throw new cgAppException(401, "User LogOut");
		}
	}

	public boolean authorize(String type, String currentSchema, String name,
			String action, String token) throws NumberFormatException,
			cgAppException, JSONException {
		
		
		if (!enabled){
			return true;
		}
		cgProcedure proc = new cgProcedure(name, currentSchema);
		String schema = proc.getSchema();
		if (!schema.isEmpty()) {
			schema = cgLanguage.concat("/", schema);
		} else {
			schema = cgLanguage.concat("/", currentSchema);
		}
		String resource = cgLanguage.concat(schema, "/", type, "/",
				proc.getName());

		
		//if (!isTokenValid(token)) {
		//	throw new cgAppException(401, "Session Expired");
		//}
		
		
		JSONObject queryParams = new JSONObject();
		queryParams.put("path", resource);
		queryParams.put("action", action);
		//queryParams.add("subjectid", token);
		JSONObject cookies = new JSONObject();
		cookies.put("JSESSIONID", token);
		try {
 			String content = getResource(cgLanguage.concat(uri, "/authorize"),queryParams, cookies).getString("content");
 			JSONObject result = new JSONObject(content);
 			return result.getBoolean("result");
		} catch (JSONException e) {
			return false;
		}
	}

	/*
	public ServletContext getServletContext(){
		return this.sc;
	}
	

	
	private String managePolicy(String type,String realm,Policies policies, String token) throws JAXBException, NumberFormatException, cgAppException{
        String policyStr = cgLanguage.objToXML(policies);
        MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("cmd", type);
		queryParams.add("realm", realm);
		queryParams.add("xmlfile",policyStr);
		queryParams.add("submit", "");
        
        ArrayList<Cookie> cookies= new ArrayList<Cookie>();
		cookies.add(new Cookie("iPlanetDirectoryPro", token));
		return getResource(this.uri.replace("identity", "ssoadm1.jsp"), queryParams, cookies).getContent();
	}
	
	public String createPolicy(String realm, Policies policies, String token) throws NumberFormatException, JAXBException, cgAppException{
		String result = managePolicy("create-policies", realm, policies, token);
		if (result.indexOf("already exists") != -1){
			result = updatePolicy(realm, policies, token);
		}
		return result;
	}
	
	public String updatePolicy(String realm, Policies policies, String token) throws NumberFormatException, JAXBException, cgAppException{
		String result = managePolicy("update-policies", realm, policies, token);
		return result;		
	}
	
	public String listPolicy(String realm, String policy, String token) throws cgAppException{
		return listPolicy(realm, new String[]{policy}, token);
	}
	
	public String listPolicy(String realm, String[] policies, String token) throws cgAppException{
        MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		for (String str : policies) {
			queryParams.add("policynames", str);
		}
		queryParams.add("cmd", "list-policies");
		queryParams.add("realm", realm);
		queryParams.add("policynameslblb", "");
		queryParams.add("submit", "");
        
        ArrayList<Cookie> cookies= new ArrayList<Cookie>();
		cookies.add(new Cookie("iPlanetDirectoryPro", token));
		
		String result = getResource(this.uri.replace("identity", "ssoadm1.jsp"), queryParams, cookies).getContent();
		
		return result;
	}

	public JSONArray list(String schema, String type, String token) throws cgAppException, JSONException{
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		
		queryParams.add("attributes_names", "realm");
		queryParams.add("attributes_values_realm", cgLanguage.concat("/",schema));
		queryParams.add("attributes_names", "objecttype");
		queryParams.add("attributes_values_objecttype", "group");
		
		queryParams.add("admin", token);

			String content = getResource(
					cgLanguage.concat(uri, "/json/search"), queryParams)
					.getContent();
		
		//http://sso.itineris.do/sso/identity/json/search?attributes_names=realm&attributes_values_realm=/MSTNMEDIA&attributes_names=objecttype&attributes_values_objecttype=group&admin=AQIC5wM2LY4Sfcx_bjuIPC0FwsHA6xkpzcv95kTnzLWkWrQ.*AAJTSQACMDIAAlNLABQtMjIyMDc1OTUzODc2ODAwNjQ1NwACUzEAAjAx*
		
		
		JSONArray list = (new JSONObject(content)).getJSONArray("string");
		return list;
	}

	public String addPolicyReferral2(String schema, String policy, String referral, String token) throws ExecuteException, IOException{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		//CommandLine pr1 = CommandLine.parse("ssh -i /home/ec2-user/mstnmedia.pem ec2-user@184.72.122.80 'ls'");
		DefaultExecutor executor = new DefaultExecutor();
		executor.setStreamHandler(streamHandler);

		//int exitValue = executor.execute(pr1);
		
		return outputStream.toString();
	}
	
	public String addPolicyReferral(String schema, String policy, String referral, String token) throws cgAppException, IOException, InterruptedException{
		boolean debug = true;		
		String[] commands = {"ssh","-t","-t","-i","/home/ec2-user/mstnmedia.pem","ec2-user@10.0.2.201","sudo","/data/apps/clearg/addPolicyReferral",referral.toLowerCase(),schema,policy};
		// execute addDB
		Process p = Runtime.getRuntime().exec(commands);
		p.waitFor();
		StringBuilder result = new StringBuilder();
		if (debug) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String line = null;
			
			while ((line = in.readLine()) != null) {
				result.append(line);
				result.append((char)13);
			}

			if (p.exitValue() != 0) {
				in = new BufferedReader(new InputStreamReader(
						p.getErrorStream()));
				line = null;
				while ((line = in.readLine()) != null) {
					result.append(line);
				}
			}
		}
		result.append(p.exitValue());
		
		return result.toString();
	}
*/
	public JSONObject currentUser(String token) throws NumberFormatException, cgAppException, JSONException{
		if (!enabled){
			return new JSONObject("{\"UID\":\"user\"}");
		}
		JSONObject queryParams = new JSONObject();
		queryParams.put("subjectId", token);
		
		JSONObject cookies = new JSONObject();
		cookies.put("JSESSIONID", token);
		String content = getResource(cgLanguage.concat(uri, "/currentUser"),
				queryParams, cookies).getString("content");
		JSONObject result = new JSONObject(content);
		return result;
	}
}
