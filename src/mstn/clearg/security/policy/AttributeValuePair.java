package mstn.clearg.security.policy;

import javax.xml.bind.annotation.XmlElement;

public class AttributeValuePair{

	private String name;
	private Rule attribute;
	private String value;
	
	
	public AttributeValuePair(){
		this.attribute = new Rule();
	}
	public AttributeValuePair(String name){
		this.attribute = new Rule(name);
		this.setName(name);
	}
	@XmlElement(name="Attribute")
	public Rule getAttribute() {
		return attribute;
	}
	public void setAttribute(Rule attribute) {
		this.attribute = attribute;
	}
	@XmlElement(name="Value")
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public void setName(String name) {
		this.name = name.toUpperCase();
		StringBuilder str = new StringBuilder();
		str.append("o=");
		str.append(this.name);
		str.append(",ou=services,dc=mstn,dc=com");
		value = str.toString();

	}
	
}
