package mstn.clearg.cgapp.webservices;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.servlet.ServletConfig;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

//import com.sun.jersey.spi.resource.Singleton;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.cgAppResult;
import mstn.clearg.security.cgSecurity;

@ApplicationScoped
@Path("/login")
public class login {
    @Context
    private UriInfo context;

    @Context ServletConfig sc;
    
    private cgSecurity cgsecurity;
    /**
     * Default constructor. 
     */
    
    public login() {

    }
    
    @PostConstruct
    public void init(){
    	this.cgsecurity = new cgSecurity();
    }

    /**
     * Retrieves representation of an instance of login
     * @return an instance of String
     */
    @GET
    @Produces("application/json")
    public Response getJson(@QueryParam("username") String username, @QueryParam("password") String password, @QueryParam("schema") String schema, @QueryParam("token") String token, @QueryParam("callback") String callback) {
    	cgAppResult result = new cgAppResult("login");
		try {
			result = cgsecurity.login(username, password,schema,token);
			if (!result.getStatus().equals("200")){
				result.setContent("Invalid username / password");
			} else {
				result.setContent(result.getContent().replace("token.id=","XXX"));
			}
			
		} catch (NumberFormatException e) {
    		result.setStatus(500);
    		result.setContent(e.getMessage());
		} catch (cgAppException e) {
    		result.setStatus(e.getStatus());
    		if (e.getStatus() != 401){
    			result.setContent(e.getMessage());
    		}
		} catch (Exception e){
			result.setStatus(500);
			result.setContent("ws:login " + e.toString());
		}
				
		NewCookie cookie = new NewCookie("iPlanetDirectoryPro", result.getContent(),"/",".itineris.do","",3600,false);
		
		javax.ws.rs.core.Response cookieResponse = Response.ok(result.toString()).cookie(cookie).build();
    	return cookieResponse;
    	//TODO Implement callback
		/*
    	if (callback != null){
			//return cgLanguage.concat(callback,"(" ,result.toString() , ")");
		} else {
			
			
			
		}
		*/
    }

    /**
     * PUT method for updating or creating an instance of login
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response postJson(@FormParam("username") String username, @FormParam("password") String password, @FormParam("schema") String schema, @FormParam("token") String token, @FormParam("callback") String callback) {
        return getJson(username,password,schema, token, callback);
    }    
}