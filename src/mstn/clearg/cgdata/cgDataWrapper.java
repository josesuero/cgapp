package mstn.clearg.cgdata;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.collections.cgArgs;
import mstn.clearg.collections.cgHashmap;
import mstn.clearg.collections.cgQueryList;
import mstn.clearg.collections.cgRowSet;
import mstn.clearg.collections.cgRowSets;
import mstn.clearg.language.cgLanguage;
import mstn.clearg.language.cgProcedure;
import mstn.clearg.security.cgSecurity;

import org.apache.ddlutils.model.*;
import org.apache.ddlutils.platform.mysql.MySql50Platform;

@SuppressWarnings("unchecked")
public class cgDataWrapper {

	private String schema;
	private String token;
	private cgSecurity cgSecurity;
	private HashMap<String, Integer> sequences = new cgHashmap<String, Integer>();
	private Connection tranConn;
	private boolean tranOpened = false; 
	//private HashMap<String, DataSource> pools;
	//private ServletContext sc;
	
	public String getSchema() {
		return schema;
	}

	/*
	 * public void setSchema(String schema) { this.schema = schema; }
	 */
	public cgDataWrapper(String schema, String token, cgSecurity cgSecurity) throws cgAppException, NumberFormatException, JSONException {
		this.schema = schema;
		this.token = token;
		this.cgSecurity = cgSecurity;
		//this.sc = cgSecurity.getServletContext();
		//this.pools = (HashMap<String, DataSource>)cgSecurity.getServletContext().getAttribute("pools");
		if (!this.schema.equalsIgnoreCase("NONE") && !cgSecurity.authorize("SCHEMA", this.schema, cgLanguage.concat(schema , "."), "GET",
				token)) {
			throw new cgAppException(403, cgLanguage.concat("Access denied: SCHEMA " , schema));
		}
	}
	
	public cgDataWrapper(String schema, String token)
			throws cgAppException {
		this.schema = schema;
		this.token = token;
		//this.sc = sc;
		//pools = (HashMap<String, DataSource>)sc.getAttribute("pools");
	}
/*
    public DataSource getPool(String poolName, String dbType){
    	String fullname = cgLanguage.concat(poolName, dbType);
    	if (pools.containsKey(fullname)){
    		return pools.get(fullname);
    	} else {
	    	PoolProperties p = new PoolProperties();
	        p.setUrl(cgLanguage.concat(sc.getInitParameter(cgLanguage.concat("jdbc",dbType)),poolName));
	        p.setDriverClassName("com.mysql.jdbc.Driver");
	        p.setUsername(poolName.toLowerCase());
	        p.setPassword(sc.getInitParameter("jdbcPass"));
	        p.setJmxEnabled(true);
	        p.setTestWhileIdle(true);
	        p.setTestOnBorrow(true);
	        p.setValidationQuery("SELECT 1");
	        p.setTestOnReturn(false);
	        p.setValidationInterval(30000);
	        p.setTimeBetweenEvictionRunsMillis(30000);
	        p.setMaxActive(100);
	        p.setInitialSize(0);
	        p.setMaxWait(10000);
	        p.setRemoveAbandonedTimeout(20);
	        p.setMinEvictableIdleTimeMillis(30000);
	        p.setMinIdle(0);
	        p.setLogAbandoned(true);
	        p.setRemoveAbandoned(true);
	        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
	          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
	        DataSource datasource = new DataSource();
	        datasource.setPoolProperties(p);
	        pools.put(fullname, datasource);
	        return datasource;
    	}
    }
*/	
	private Connection getDataSource(String type, String schema)
			throws NamingException, SQLException {

		if (this.tranOpened){
			return this.tranConn;
		}
        //Connection conn = null;
        /*
        DataSource datasource = getPool(schema, type);
        conn = datasource.getConnection();
        return conn;
        */
		//Tomcat ConnPool
		

		//JNDI
		
		DataSource clearg;
		InitialContext ctx = new InitialContext();
		//String datasource = cgLanguage.concat("java:jdbc/" , schema.toUpperCase() , type.toUpperCase());
		//TODO Consider reading writting implemantation
		String datasource = cgLanguage.concat("java:/jdbc/" , schema.toUpperCase());
		clearg = (DataSource) ctx.lookup(datasource); 
		return clearg.getConnection();
		
		
		//JDBC
		/*
		String jdbcurl = "";
		if (type.equalsIgnoreCase("READ")){
			jdbcurl = cgLanguage.concat("jdbc:mysql://localhost:3306/", schema);
		} else if (type.equalsIgnoreCase("WRITE")){
			jdbcurl = cgLanguage.concat("jdbc:mysql://localhost:3306/",schema);
		}
		
		Properties connectionProps = new Properties();
	    connectionProps.put("user", "root");
	    connectionProps.put("password", "slayer");
		Connection conn = null;
        conn = DriverManager.getConnection(jdbcurl,connectionProps);
		return conn;
		*/
	}
	
	private void closeDataSource(Connection conn) throws SQLException{
		if (!tranOpened){
			conn.close();
		}
	}
	
	public void tranStart() throws NamingException, SQLException{
		this.tranConn = getDataSource("WRITE", this.schema);
		this.tranConn.setAutoCommit(false);
		this.tranOpened = true;
	}
	
	public void tranEnd() throws SQLException{
		this.tranOpened = false;
		this.tranConn.setAutoCommit(true);
		this.tranConn.close(); 
	}
	
	public boolean tranStatus(){
		return this.tranOpened;
	}
	
	public void commit() throws SQLException{
		this.tranConn.commit();
		tranEnd();
	}
	
	public void rollback() throws SQLException{
		this.tranConn.rollback();
		tranEnd();
	}
	
	public cgRowSet query(String sqlstr) throws SQLException,
			UnsupportedEncodingException, NamingException, JSONException {
		return query(sqlstr, new cgArgs());
	}

	public cgRowSet query(String sqlstr, cgArgs args) throws SQLException,
			UnsupportedEncodingException, NamingException, JSONException {
		Connection conn = getDataSource("READ", schema);
		try {
			return cgSQL.query(conn, sqlstr, args);
		} finally {
			closeDataSource(conn);
		}
	}

	public boolean noquery(String sqlstr) throws SQLException,
			UnsupportedEncodingException, NamingException, JSONException {
		return noquery(sqlstr, new cgArgs());
	}

	public boolean noquery(String sqlstr, cgArgs args) throws SQLException,
			UnsupportedEncodingException, NamingException, JSONException {
		Connection conn = getDataSource("WRITE", schema);
		try {
			return cgSQL.noquery(conn, sqlstr, args);
		} finally {
			closeDataSource(conn);
		}
	}

	public ArrayList<Integer> noquerybatch(String... sqlstr)
			throws SQLException, NamingException {
		cgQueryList list = new cgQueryList();
		for (String sql : sqlstr) {
			list.add(sql, new cgArgs(), false);
		}
		return noquerybatch(list);
	}

	public ArrayList<Integer> noquerybatch(cgQueryList list)
			throws SQLException, NamingException {
		Connection conn = getDataSource("WRITE", schema);
		try {
			return cgSQL.noquerybatch(conn, list);
		} finally {
			closeDataSource(conn);
		}
	}

	public String queryJSON(String sqlstr, boolean meta) throws SQLException,
			UnsupportedEncodingException, JSONException, NamingException {
		return query(sqlstr).toJSONString(meta);
	}

	public cgRowSet ExecProcedureDataSet(String procedure, cgArgs args)
			throws SQLException, NamingException, UnsupportedEncodingException,
			JSONException {
		Connection conn = getDataSource("READ", schema);
		try {
			return cgSQL.ExecProcedureDataSet(conn, this.schema, procedure,
					args);
		} finally {
			closeDataSource(conn);
		}
	}

	public Boolean ExecProcedureNoquery(String procedure, cgArgs args)	
			throws SQLException, NamingException {
		Connection conn = getDataSource("WRITE", schema);
		try {
			return cgSQL.ExecProcedureNoquery(conn, this.schema, procedure,
					args);
		} finally {
			closeDataSource(conn);
		}
	}

	public String ExecProcedureJSON(String procedure, cgArgs args)
			throws SQLException, NamingException, UnsupportedEncodingException,
			JSONException {
		cgRowSet rs = ExecProcedureDataSet(procedure, args);
		if (!(rs == null)) {
			return rs.toJSONString();
		} else {
			return "";
		}
	}

	public String datacollection(String[] queries) throws SQLException,
			UnsupportedEncodingException, NamingException, JSONException {
		cgRowSets rsc = new cgRowSets();
		for (String sqlstr : queries) {
			cgRowSet rs = query(sqlstr);
			rsc.add(rs);
		}
		return rsc.toJSONString();
	}

	public cgRowSet getCGTypes() throws UnsupportedEncodingException,
			JSONException, SQLException, NamingException {
		Connection conn = getDataSource("READ", "CLEARG");
		try {
			return cgSQL.query(conn, "SELECT * FROM cg_datatypes", new cgArgs());
		} finally {
			closeDataSource(conn);
		}

	}

	public boolean updateDatabase(JSONArray tables) throws SQLException,
			JSONException, UnsupportedEncodingException, NamingException,
			cgAppException {
		Connection conn = null;
		MySql50Platform platform = new MySql50Platform();
		cgQueryList list = new cgQueryList();
		JSONArray types = getCGTypes().data();

		for (int i = 0; i < tables.length(); i++) {
			cgProcedure tblName = (new cgProcedure(tables.getString(i),
					this.schema));
			// Authorize Table
			if (!cgSecurity.authorize("TABLES", this.schema,
					tables.getString(i), "GET", this.token)) {
				throw new cgAppException(403, cgLanguage.concat("Access denied: TABLE " , tables.getString(i)));
			}

			// Get Table Definition
			conn = getDataSource("READ", tblName.getObjSchema());
			try {
				JSONArray tblData = cgSQL
						.query(conn,
								"SELECT name,fields, options, version from cg_tables where name = ?",
								(new cgArgs()).add(tblName.getName(), 3))
						.data();
				if (tblData.length() == 0) {
					throw new cgAppException(404, cgLanguage.concat("Table ",tblName.getFullName() , " not found"));
				}

				JSONObject tbl = tblData.getJSONObject(0);

				String sqlstr = "insert into cg_tables(name,fields, options, version,tbltype) values(?,?,?, ?,?) on duplicate key update name = ?, fields = ?, options = ?, version = ?, tbltype=?";

				cgArgs args = new cgArgs();
				args.add(tblName.getFullName().toUpperCase(), 3);
				args.add(tbl.getString("FIELDS"), 15);
				args.add(tbl.getString("OPTIONS"), 15);
				args.add(tbl.getString("VERSION"), 2);
				args.add("2", 2);

				args.add(tblName.getFullName().toUpperCase(), 3);
				args.add(tbl.getString("FIELDS"), 15);
				args.add(tbl.getString("OPTIONS"), 15);
				args.add(tbl.getString("VERSION"), 2);
				args.add("2", 2);

				list.add(sqlstr, args, false);
			} catch(JSONException ex){
				throw new JSONException(cgLanguage.concat("Table" ,tblName.getFullName() ,ex.getMessage(), " ",ex.getCause().toString()));
			} finally {
				closeDataSource(conn);
			}

		}

		Database database = new Database();
		database.setName(this.schema);
		Table table;

		conn = getDataSource("WRITE", this.schema);
		String tblName = "";
		try {
			noquerybatch(list);

			String sqlstr = "select NAME,FIELDS,OPTIONS from cg_tables where tbltype in (1,2)";
			tables = query(sqlstr).data();

			for (int i = 0; i < tables.length(); i++) {
				JSONObject tbl = tables.getJSONObject(i);
				tblName = tbl.getString("NAME");
				table = new Table();
				table.setName(new cgProcedure(tblName, this.schema).getName().toLowerCase());

				JSONArray fields = tbl.getJSONArray("FIELDS");

				for (int x = 0; x < fields.length(); x++) {
					JSONObject field = fields.getJSONObject(x);
					String typeInt = field.getString("type");
					if (typeInt.equals("0")) {
						continue;
					}

					Column column = new Column();
					column.setName(field.getString("fieldname").toUpperCase());

					if (typeInt.equals("1")) {
						column.setAutoIncrement(true);
					}

					String type = types.getJSONObject(types.find("ID", typeInt)).getString("SQLFIELD");
					column.setType(type);
					
					column.setSizeAndScale(field.getInt("size"), field.getInt("scale"));

					int index = field.getInt("index");

					if (index == 1) {
						column.setPrimaryKey(true);
					} else if (index == 2) {
						NonUniqueIndex indexObj = new NonUniqueIndex();
						indexObj.setName(cgLanguage.concat("idx",field.getString("fieldname")));
						indexObj.addColumn(new IndexColumn(column));
						table.addIndex(indexObj);
					} else if (index == 3) {
						UniqueIndex indexObj = new UniqueIndex();
						indexObj.setName(cgLanguage.concat("idx",field.getString("fieldname")));
						indexObj.addColumn(new IndexColumn(column));
						table.addIndex(indexObj);
					}
					table.addColumn(column);
				}
				JSONObject options = tbl.getJSONObject("OPTIONS");
				if (options.has("indexes")) {
					JSONArray indexes = options.getJSONArray("indexes");
					for (int y = 0; y < indexes.length(); y++) {
						JSONObject index = indexes.getJSONObject(y);
						if (index.getInt("type") == 2) {
							NonUniqueIndex indexObj = new NonUniqueIndex();
							indexObj.setName(cgLanguage.concat("idx",index.getString("name")));
							JSONArray indexfields = index.getJSONArray("fields");
							for (int z = 0; z < indexfields.length(); z++) {
								indexObj.addColumn(new IndexColumn(indexfields.getString(z)));
							}
							table.addIndex(indexObj);
						} else if (index.getInt("type") == 3) {
							UniqueIndex indexObj = new UniqueIndex();
							indexObj.setName(cgLanguage.concat("idx",index.getString("name")));
							JSONArray indexfields = index.getJSONArray("fields");
							for (int z = 0; z < indexfields.length(); z++) {
								indexObj.addColumn(new IndexColumn(indexfields.getString(z)));
							}
							table.addIndex(indexObj);
						}
					}
				}

				database.addTable(table);
			}
			platform.alterTables(conn, database, false);
			return true;
		} catch (JSONException e){
			throw new JSONException(cgLanguage.concat("Tbl: ",tblName," ",e.getMessage()));
		} finally {
			closeDataSource(conn);
		}
	}

	public boolean updateDatabase(String... tables) throws SQLException,
			JSONException, UnsupportedEncodingException, NamingException,
			cgAppException {
		JSONArray tbls = new JSONArray();
		for (String table : tables) {
			tbls.add(table);
		}
		return updateDatabase(new JSONArray(tbls));
	}

	// Sequence Functions
	public int getSequence(String sequence) {
		int value;
		if (sequences.containsKey(sequence)) {
			value = sequences.get(sequence);
			value++;
		} else {
			value = 1;
		}
		sequences.put(sequence, value);
		return value;
	}

	public void updateSequence(String sequence, int max, int min, int interval,
			int cachesize, int cycle, int currentvalue) {
	}

	public void resetSequence(String sequence, int initValue) {
		int value;
		if (sequences.containsKey(sequence)) {
			value = sequences.get(sequence);
			value++;
		} else {
			value = 1;
		}
		sequences.put(sequence, value);
	}
}