package mstn.clearg.collections;

import java.util.Iterator;
import java.util.LinkedList;

import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.annotations.JSFunction;

public class cgQueryList extends ScriptableObject{

	private LinkedList<cgQueryObj> list;
	private static final long serialVersionUID = -7917325391950453326L;
	
	public cgQueryList(){
		list = new LinkedList<cgQueryObj>();
	}
	
	@JSFunction
	public cgQueryList addQuery(cgQueryObj obj){
		if (!obj.getsqlstr().isEmpty())
			list.add(obj);
		return this;
	}
	
	@JSFunction
	public cgQueryList add(String sqlstr, cgArgs args, boolean returnId){
		list.add(new cgQueryObj(sqlstr, args,returnId));
		return this;
	}
	@JSFunction
	public cgQueryList addQueryList(cgQueryList list){
		Iterator<cgQueryObj> i = list.getList().iterator();
		while (i.hasNext()){
			addQuery(i.next());
		}
		return this;
	}
	
	@JSFunction
	public LinkedList<cgQueryObj> getList(){
		return list;
	}
	
	@JSFunction
	public int size(){
		return list.size();
	}
	
	@JSFunction
	public cgQueryObj get(int index){
		return list.get(index);
	}

	@Override
	public String getClassName() {
		return "cgQueryList";
	}

}
