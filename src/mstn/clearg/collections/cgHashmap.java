package mstn.clearg.collections;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;

import mstn.clearg.language.cgLanguage;

/**
 * Collection Class for clearg
 * @author MSTN MEdia
 *
 * @param <K> Key Type
 * @param <V> Value Type
 */
public class cgHashmap<K,V> extends HashMap<K,V>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String toJson(){
		StringWriter out = new StringWriter();
		Iterator<K> it = this.keySet().iterator();
		boolean first = true;
		out.append("{");
		while (it.hasNext()){
			K name = it.next();
			Object value = this.get(name);
			if (!first){
				out.append(",");
			} else {
				first = false;
			}
			out.append(jsonString(name,value));
		}
		out.append("}");
		return out.toString();
	}

	private String jsonString(K name, Object value){
		StringWriter out = new StringWriter();
		if (value.getClass().getName().equals("java.lang.String")){
			//Normal
			out.append(cgLanguage.concat("\"",name.toString() ,"\""));
			out.append(":");
			out.append(cgLanguage.concat("\"", value.toString() , "\""));
		} else {
			out.append(cgLanguage.concat("\"" ,name.toString() , "\"" , ":"));
			
			//cgCollection<String, ArrayList<String>> testdata = new cgCollection<String, ArrayList<String>>();
			//testdata.put(name, (ArrayList<String>)value);

			String replacedValue = value.toString()
			.replace("[","[\"").replace(", ","\",\"").replace("]", "\"]")
			.replace("=","\":\"")
			.replace("{","{\"")
			.replace("}","\"}");
			out.append(replacedValue);
		}
		
		return out.toString();
	}

}
