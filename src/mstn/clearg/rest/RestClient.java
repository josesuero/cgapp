package mstn.clearg.rest;

import javax.ws.rs.core.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

@SuppressWarnings("rawtypes")
public class RestClient {

	// CONSTRUCTOR
	public RestClient() {

	}
/**
 * Function para hacer requerimientos a WebService REST
 * @param client
 * @param requestObj JSONObject opciones:
 * url - String
 * headers - JSONArray {name,value}
 * params - JSONArray {name,value}
 * accept - String
 * type - String
 * entity - String
 * cookies - JSONArray {name,value}
 * method - String  
 * @return
 * @throws JSONException
 * 
 * 
 */

	public static JSONObject request(Client client, JSONObject requestObj) throws JSONException {

			//Client client = ClientBuilder.newClient();
			String url = requestObj.getString("url");
			// , String method, JSONArray headers, acceptMediaType

			
			
			
			
			WebTarget webResource = client.target(url);
			
			String type = "text/plain";
			String entitytype= "Text/Plain";
			
			if (requestObj.has("type")){
				type = requestObj.getString("type");
				entitytype = type;
			}			
			
			//WebResource webResource = client.resource(url);
			
			if (requestObj.has("params")) {
				JSONObject params = requestObj.getJSONObject("params");
				for (Iterator iter = params.keys(); iter.hasNext(); ) {
					String key = iter.next().toString();
					webResource = webResource.queryParam(key, params.getString(key));
				}
			}
			
			Builder builder = webResource.request(type);
			
			// .resource("http://localhost:8080/JerseyWebService/rest/WebService");
			if (requestObj.has("headers")) {
				JSONArray headers = requestObj.getJSONArray("headers");
				for (int i = 0; i < headers.length(); i++) {
					JSONObject header = headers.getJSONObject(i);
					builder = builder.header(header.getString("name"),
							header.getString("value"));
				}
			}


						
			if (requestObj.has("accept")){
				builder = builder.accept(requestObj.getString("accept"));	
			}
			
			
			
			if (requestObj.has("entity")){
				entitytype = requestObj.getString("entity");
			}

			if (requestObj.has("cookies")){
				JSONObject cookies = requestObj.getJSONObject("cookies");
				for (Iterator iter = cookies.keys(); iter.hasNext(); ) {
					String key = iter.next().toString();
					builder = builder.cookie(new Cookie(key, cookies.getString(key)));
				}
			}
			if (!requestObj.has("method")){
				requestObj.put("method","get");
			}
			Response response;
			if (requestObj.getString("method").toLowerCase().equals("put")){
				response = builder.put(Entity.entity(entitytype,type));
			} else if (requestObj.getString("method").toLowerCase().equals("post")){
				response = builder.post(Entity.entity(entitytype,type));
			} else if (requestObj.getString("method").toLowerCase().equals("delete")){
				response = builder.delete(Response.class);
			} else {
				response = builder.get(Response.class);
			}
			JSONObject output = new JSONObject();
			output.put("content", response.readEntity(String.class));
			output.put("status", response.getStatus());
			response.close();
			//client.close();
			return output;
	}

}