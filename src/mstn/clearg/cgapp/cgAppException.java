package mstn.clearg.cgapp;

public class cgAppException extends Exception {
	private static final long serialVersionUID = 2393000319433792107L;

	private int status;
	private String content;
	
	public cgAppException(int status, String content){
		super(content);
		this.status = status;
	}
	
	public int getStatus(){
		return this.status;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public String getContent(){
		return this.content;
	}
	
	public void setContent(String content){
		this.content = content;
	}
}
