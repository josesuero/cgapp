package mstn.clearg.cgapp.webservices;

import javax.servlet.ServletConfig;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.inject.Singleton;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.ddlutils.DatabaseOperationException;
import org.json.JSONException;
import org.mozilla.javascript.WrappedException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
//import com.sun.jersey.spi.resource.Singleton;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.cgAppResult;
//import mstn.clearg.cgapp.cgSequence.cgSequenceList;
import mstn.clearg.language.cgLanguage;
import mstn.clearg.security.cgSecurity;

@Singleton
@Startup
@Path("server")
public class server {
    private cgSecurity cgsecurity;
//    private cgSequenceList seqList;
    
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    @Context ServletConfig sc; 
    /**
     * Default constructor. 
     */
    public server() {
    	//this.cgsecurity = new cgSecurity("http://localhost:8080/sso/identity");
    	//this.cgsecurity = new cgSecurity(wc.getInitParameter("ssouri"));    	
    }
    
    @PostConstruct
    public void init(){
    	/*
    	if (sc.getServletContext().getAttribute("pools") == null){
        	sc.getServletContext().setAttribute("pools", new HashMap<String, DataSource>());    	
    	}
    	*/
    	//this.cgsecurity = new cgSecurity(sc.getServletContext().getInitParameter("ssouri"));
    	this.cgsecurity = new cgSecurity();
    	
    }

    
    public void params(){
    	
    }
    

    /**
     * Retrieves representation of an instance of server
     * @return an instance of String
     */

    public cgAppResult process(String procedure, String token, String data, String schema, String callback, String cookietoken) {
    	long starttime = System.currentTimeMillis();
    	
   	cgAppResult result = new cgAppResult("server");
    	
    	if (token == null){
			token = cookietoken;
    	}
    	
    	try {
    		Object processResult = "";
        	//Exec Procedure
    		cgLanguage cglang = new cgLanguage(token, schema, data, cgsecurity);
    		processResult = cglang.execprocedure(procedure);

    		result.setStatus(200);
    		result.setContent(processResult);
    	} catch(cgAppException e) {
    		result.setStatus(e.getStatus());
    		result.setContent("Authentication credentials were missing or incorrect");
    		if (e.getStatus() != 401){
    			result.setContent(e.getMessage());
    		}
		} catch (WrappedException e){
			Exception ex = (Exception)e.getWrappedException();
			result.setStatus(500);
			if (ex instanceof cgAppException){
	    		result.setStatus(((cgAppException)ex).getStatus());
	    		result.setContent(cgLanguage.concat(((cgAppException)ex).getMessage(), "(" , e.sourceName() , ":" , String.valueOf(e.lineNumber()) , ")" ));
			}
	    	if (ex instanceof DatabaseOperationException){
	    		result.setContent(((DatabaseOperationException)ex).getMessage());
			} else {
				String message = ex.getMessage();
				if (message == null){
					message = ex.toString();
				}
				result.setContent(cgLanguage.concat(message, "(", e.sourceName() , ":" , String.valueOf(e.lineNumber()) , ")" ));
			}
		} catch(MySQLSyntaxErrorException e){
			result.setContent(e.getMessage());
	    	result.setStatus(500);
		} catch(JSONException e){
			result.setContent(e.getMessage());
	    	result.setStatus(500);
		} catch (Exception e){
    		StackTraceElement[] trace = e.getStackTrace();
    		String content = e.toString();
    		
    		for (StackTraceElement stack : trace){
    			content = cgLanguage.concat(content,stack.toString());
    		}
			result.setContent(content);
			result.setStatus(500);
    	}

    	long endttime = System.currentTimeMillis();
    	result.setTime(endttime - starttime);
		
		return result;	
    }

    @GET
    @Produces("application/json")
    public String getJson(@QueryParam("procedure") String procedure, @QueryParam("token") String token, @QueryParam("data") String data, @QueryParam("schema") String schema, @QueryParam("callback") String callback, @CookieParam("iPlanetDirectoryPro") String cookietoken) {
    	return process(procedure, token, data, schema, callback,cookietoken).toString();
    }
    
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public String postJson(@FormParam("procedure") String procedure, @FormParam("token") String token, @FormParam("data") String data, @FormParam("schema") String schema, @FormParam("callback") String callback, @CookieParam("iPlanetDirectoryPro") String cookietoken) {
        return process(procedure, token, data, schema, callback,cookietoken).toString();
    }
    
    @GET
    @Path("/{procedure}") 
    @Produces("application/javascript")
    public String getprocedure(@PathParam("procedure") String procedure, @QueryParam("token") String token, @QueryParam("data") String data, @QueryParam("schema") String schema, @QueryParam("callback") String callback, @CookieParam("iPlanetDirectoryPro") String cookietoken) {
    	return process(procedure, token, data, schema, callback, cookietoken).toString();
    }
    
    @GET
    @Path("/{type:.+}/{procedure}") 
    @Produces("application/javascript")
    public Response getjspath(@PathParam("procedure") String procedure, @QueryParam("token") String token, @QueryParam("data") String data, @QueryParam("schema") String schema, @QueryParam("callback") String callback, @CookieParam("iPlanetDirectoryPro") String cookietoken, @PathParam("type") String type) {
    	String content = process(procedure, token, data, schema, callback, cookietoken).getContent();
    	String mediaType = "aplication/json";
    	if (type.equalsIgnoreCase("json")){
    		mediaType = MediaType.APPLICATION_JSON;
    	} else if (type.equalsIgnoreCase("html")) {
    		mediaType = MediaType.TEXT_HTML;
    	} else if (type.equalsIgnoreCase("text")){
    		mediaType = MediaType.TEXT_PLAIN;
    	} else if (type.equalsIgnoreCase("xml")){
    		mediaType = MediaType.APPLICATION_XML;
    	}
    	return Response.ok(content).type(mediaType).build();
    }
    
    @GET
    @Path("/jsonp")
    @Produces("application/json")
    public String getjsonppath(@QueryParam("procedure") String procedure, @QueryParam("token") String token, @QueryParam("data") String data, @QueryParam("schema") String schema, @QueryParam("callback") String callback, @CookieParam("iPlanetDirectoryPro") String cookietoken) {
    	return cgLanguage.concat(callback , "(" , process(procedure, token, data, schema, callback,cookietoken).toString() , ")");
    }
    
}