package mstn.clearg.language;

import org.mozilla.javascript.*;

@SuppressWarnings({"rawtypes"})
public class cgContextFactory extends ContextFactory
{
	
	static final long serialVersionUID = 1L;
	
	public static class SandboxNativeJavaObject extends NativeJavaObject {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SandboxNativeJavaObject(Scriptable scope, Object javaObject, Class staticType) {
			super(scope, javaObject, staticType);
		}
	 
		@Override
		public Object get(String name, Scriptable start) {
			if (name.equals("getClass")) {
				return NOT_FOUND;
			}
	 
			return super.get(name, start);
		}
	}

	public static class SandboxWrapFactory extends WrapFactory {
		@Override
		public Scriptable wrapAsJavaObject(Context cx, Scriptable scope, Object javaObject, Class staticType) {
			return new SandboxNativeJavaObject(scope, javaObject, staticType);
		}
	}

    // Custom Context to store execution time.
	
    static {
        // Initialize GlobalFactory with custom factory
    	if (!(ContextFactory.getGlobal().getClass().getSimpleName().equals("cgContextFactory"))){
    		ContextFactory.initGlobal(new cgContextFactory());
    	}
    }
    

    // Override makeContext()
    protected Context makeContext()
    {
        cgContext cx = new cgContext();
        // Use pure interpreter mode to allow for
        // observeInstructionCount(Context, int) to work
        cx.setOptimizationLevel(-1);
        // Make Rhino runtime to call observeInstructionCount
        // each 10000 bytecode instructions
        cx.setInstructionObserverThreshold(10000);
        /*
        cx.setClassShutter(new ClassShutter() {
        	public boolean visibleToScripts(String className) {					
        		if(className.startsWith("org.json") 
        				|| className.equals("mstn.clearg.cgdata.cgDataWrapper") 
        				|| className.startsWith("mstn.clearg.language") 
        				|| className.startsWith("java.lang"))
        			return true;
        		
        		return false;
        	}
        });
        cx.setWrapFactory(new SandboxWrapFactory());
        */
        return cx;
    }

    // Override hasFeature(Context, int)
    public boolean hasFeature(Context cx, int featureIndex)
    {
        // Turn on maximum compatibility with MSIE scripts
        switch (featureIndex) {
            case Context.FEATURE_NON_ECMA_GET_YEAR:
                return true;

            case Context.FEATURE_MEMBER_EXPR_AS_FUNCTION_NAME:
                return true;

            case Context.FEATURE_RESERVED_KEYWORD_AS_IDENTIFIER:
                return true;

            case Context.FEATURE_PARENT_PROTO_PROPERTIES:
                return false;
        }
        return super.hasFeature(cx, featureIndex);
    }

    // Override observeInstructionCount(Context, int)
    protected void observeInstructionCount(Context cx, int instructionCount)
    {
    	cgContext mcx = (cgContext)cx;
        long currentTime = System.currentTimeMillis();
        if (currentTime - mcx.startTime > 30*1000) {
            // More then 10 seconds from Context creation time:
            // it is time to stop the script.
            // Throw Error instance to ensure that script will never
            // get control back through catch or finally.
            
        	//throw new Error("TimeOut");
        }
    }

    // Override doTopCall(Callable, Context, Scriptable,Scriptable, Object[])
    protected Object doTopCall(Callable callable,
                               Context cx, Scriptable scope,
                               Scriptable thisObj, Object[] args)
    {
    	cgContext mcx = (cgContext)cx;
        mcx.startTime = System.currentTimeMillis();

        return super.doTopCall(callable, cx, scope, thisObj, args);
    }

}