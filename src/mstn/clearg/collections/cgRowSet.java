package mstn.clearg.collections;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import mstn.clearg.utilities.cgConvert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class cgRowSet extends JSONObject {
	/**
	 * 
	 */
	public cgRowSet() throws SQLException {
		super();
	}

	public String toJSONString(boolean meta) throws JSONException,SQLException, UnsupportedEncodingException{
		return toJSON(meta).toString();
	}
	public String toJSONString() throws JSONException,SQLException, UnsupportedEncodingException{
		return toJSONString(false);
	}
	public JSONObject toJSON() throws JSONException,SQLException, UnsupportedEncodingException{
		return toJSON(false);
	}
	
	public JSONObject toJSON(boolean meta) throws JSONException{
		return this;
	}
	
	public void populate(ResultSet rs) throws JSONException,SQLException, UnsupportedEncodingException{

		JSONArray metadata = new JSONArray();
		ResultSetMetaData rsmd = rs.getMetaData();

		for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
				JSONObject metaobj = new JSONObject();
				metaobj.put("name", rsmd.getColumnName(i).toUpperCase());
				metaobj.put("type", rsmd.getColumnTypeName(i));
				metaobj.put("size", rsmd.getColumnDisplaySize(i));
				metadata.put(metaobj);
			}

		this.put("meta", metadata);
		JSONArray data = new JSONArray();
		
		while (rs.next()) {
			JSONObject dataobj = new JSONObject();
			for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
				String name = rsmd.getColumnName(i).toUpperCase();
				if (rsmd.getColumnTypeName(i).equals("CLOB")) {
					if (rs.getString(i) == null) {
						dataobj.put(name, "");
					} else {
						//dataobj.put(this.getMetaData().getColumnName(i), java.net.URLEncoder.encode(cgConvert.clob2string(this.getClob(i)), "UTF-8"));
						dataobj.put(name, cgConvert.clob2string(rs.getClob(i)));
						//dataobj.put(this.getMetaData().getColumnName(i), java.net.URLEncoder.encode(dataobj.get(this.getMetaData().getColumnName(i)).toString(), "UTF-8"));
					}
				} else {
					Object value = rs.getString(i);
					if (value == null){
						value = "";
					}
					dataobj.put(name, value);
				}
			}
			data.put(dataobj);
		}

		this.put("data", data);
	}	

	public JSONArray data() throws JSONException {
		return this.getJSONArray("data");
	}
/*
	public String toJSONString(boolean meta){
		String result;
		try {
			this.beforeFirst();
			// {data:[{id:'1',name:'getSequence'},{id:'2',name:'getProcedures'}]}
			result = "{";
			result += "meta:[";

			if (meta) {
				for (int i = 1; i < this.getMetaData().getColumnCount() + 1; i++) {
					if (i > 1)
						result += ",";
					result += "{";
					result += "name:'";
					result += getMetaData().getColumnName(i);
					result += "',type:'";
					result += getMetaData().getColumnTypeName(i);
					result += "',size:";
					result += getMetaData().getColumnDisplaySize(i);
					result += "}";
				}
			}
			result += "]";
			result += ",";
			result += "data:[";
			while (this.next()) {
				if (!this.isFirst()) {
					result += ",";
				}
				result += "{";

				
				//if (this.getMetaData().getColumnCount() > 0) {
				//	result += this.getMetaData().getColumnName(1);
				//	result += ":";
				//	result += "'" + this.getString(1) + "'";
				//}
				

				for (int i = 1; i < this.getMetaData().getColumnCount() + 1; i++) {
					if (i > 1){
						result += ",";
					}
					result += this.getMetaData().getColumnName(i);
					result += ":";
					if (this.getMetaData().getColumnTypeName(i).equals("CLOB")) {
						if (this.getString(i) == null) {
							result += "''";
						} else {
							// result += "'" +
							// java.net.URLEncoder.encode(getCharacterStream(i).toString(),"UTF-8")
							// + "'";
							result += "'"
									+ java.net.URLEncoder.encode(cgConvert
											.clob2string(this.getClob(i)),
											"UTF-8") + "'";
						}
					} else {
						result += "'" + this.getString(i) + "'";
					}
				}
				result += "}";
			}
			result += "]}";
		} catch (SQLException e) {
			result = "error= " + e.getMessage();
		} catch (UnsupportedEncodingException e) {
			result = "error= " + e.getMessage();
		}

		return result;

	}
*/

}
