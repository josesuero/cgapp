package mstn.clearg.security.policy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"includeType","type","name","serviceName", "resourceName", "attributeValuePair"})
public class Rule{

	private String RuleName;
	private Rule ServiceName;
	private Rule ResourceName;
	private List<AttributeValuePair> attributeValuePair;
	private String type;
	private String includeType;
	private String description;
	
	public Rule(){
		this.RuleName = "";
		this.attributeValuePair = new ArrayList<AttributeValuePair>();
		//this.ServiceName = new Rule();
		//this.ResourceName = new Rule();
	}
	
	public Rule(String name, String service, String resource){
		this();
		this.RuleName = name;
		this.ServiceName = new Rule(service);
		this.ResourceName = new Rule(resource);
	}
	
	public Rule(String name, String resource){
		this();
		this.RuleName = name;
		this.ServiceName = new Rule("iPlanetAMWebAgentService");
		this.ResourceName = new Rule(resource);
	}
	
	public Rule(String name){
		this.RuleName = name;
	}
	
	@XmlAttribute
	public String getName(){
		return this.RuleName;
	}
	
	public void setName(String name){
		this.RuleName = name;
	}
	
	@XmlElement(name="ServiceName")
	public Rule getServiceName(){
		return this.ServiceName;
	}
	
	public void setServiceName(Rule rule){
		this.ServiceName = rule;
	}
	@XmlElement(name="ResourceName")
	public Rule getResourceName(){
		return this.ResourceName;
	}
	
	public void setResourceName(Rule rule){
		this.ResourceName = rule;
	}
	@XmlElement(name="AttributeValuePair")
	public List<AttributeValuePair> getAttributeValuePair() {
		return attributeValuePair;
	}

	public void setAttributeValuePair(List<AttributeValuePair> attributeValuePair) {
		this.attributeValuePair = attributeValuePair;
	}

	@XmlAttribute
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Rule addAttribute(String name, String value){
		AttributeValuePair attr = new AttributeValuePair(name);
		attr.setValue(value);
		this.attributeValuePair.add(attr);
		return this;
	}
	
	@XmlAttribute
	public String getIncludeType() {
		return includeType;
	}

	public void setIncludeType(String includeType) {
		this.includeType = includeType;
	}

	@XmlAttribute
	public String getDescription() {
		return description;
	}

	@XmlAttribute
	public String getDescripion() {
		return description;
	}

	
	public void setDescription(String description) {
		this.description = description;
	}
}
