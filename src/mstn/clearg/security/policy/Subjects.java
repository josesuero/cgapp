package mstn.clearg.security.policy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"includeType","description","name","subject"})
public class Subjects{

	private String name;
	private String description;
	private String includeType;
	private List<Rule> subject;

	public Subjects(){
		subject = new ArrayList<Rule>();
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlAttribute(name="description")
	public String getDescription() {
		return description;
	}

	
	
	public void setDescripion(String descripion) {
		this.description = descripion;
	}
	@XmlElement(name="Subject")
	public List<Rule> getSubject() {
		return subject;
	}

	public void setSubject(List<Rule> subject) {
		this.subject = subject;
	}
	
	public Rule addSubject(String name, String subjectType, String schema){
		Rule rule = new Rule();
		rule.setName(name);
		rule.setType("AMIdentitySubject");
		//rule.setDescription("");
		rule.setIncludeType("inclusive");
		StringBuilder identity = new StringBuilder();
		identity.append("id=");
		identity.append(name);
		identity.append(",ou=");
		identity.append(subjectType);
		identity.append(",o=");
		identity.append(schema);
		identity.append(",ou=services,dc=mstn,dc=com");
		rule.addAttribute("Values", identity.toString());
		this.subject.add(rule);
		return rule;
	}
	@XmlAttribute
	public String getIncludeType() {
		return includeType;
	}

	public void setIncludeType(String includeType) {
		this.includeType = includeType;
	}
}
