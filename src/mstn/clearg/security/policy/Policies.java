package mstn.clearg.security.policy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import mstn.clearg.language.cgLanguage;

@XmlRootElement(name="Policies")
public class Policies{

	private List<Policy> Policies;
	private String schema; 
	
	public Policies(){
		Policies = new ArrayList<Policy>();
		this.schema = "";
	}
	
	public Policies(String schema){
		this();
		this.schema = schema;
	}
	
	@XmlElement(name="Policy")
	public List<Policy> getPolicy(){
		return Policies;
	}
	
	public void setPolicy(List<Policy> Policies){
		this.Policies = Policies;
	}
	
	private String Description;
	public String getDescription(){
		return this.Description;
	}
	
	public void setDescription(String description){
		this.Description = description;
	}

	public Policy addPolicy(String name, String referral){
		Policy policy = new Policy(name,this.schema);
		policy.setReferralPolicy(referral);
		Policies.add(policy);
		return policy;
	}

	public String getXML() throws JAXBException{
		return cgLanguage.objToXML(this);
	}
	
	public String toString(){
		try {
			return getXML();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
