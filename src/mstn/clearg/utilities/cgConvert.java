package mstn.clearg.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;


public class cgConvert {
	
	public static String clob2string(Clob clob) throws SQLException{
		StringBuffer strOut = new StringBuffer();
		String aux;

		BufferedReader br = new BufferedReader(clob.getCharacterStream());
		try {
			boolean first = true;
			while ((aux = br.readLine()) != null){
				if (!first){
					strOut.append("\r\n");
				}
				strOut.append(aux);
				first = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strOut.toString();
	}
}