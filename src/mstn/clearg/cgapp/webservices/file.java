package mstn.clearg.cgapp.webservices;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.*; 

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



@Path("file")
public class file {
    @Context
    private UriInfo context;

    /**
     * Default constructor. 
     */
    public file() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Retrieves representation of an instance of fileRead
     * @return an instance of String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        // TODO return proper representation object
        return "hello";
    }

    /**
     * PUT method for updating or creating an instance of fileRead
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }

    @POST
    @Produces("text/plain")
    public String loadFile(@Context HttpServletRequest request) {
    	try {
    	      ServletFileUpload upload = new ServletFileUpload();
    	      //res.setContentType("text/plain");
    	      
    	     // PrintWriter out = response.getWriter();
    	      
    	      String typeFile = "xls";
    		  
    		  

    	      FileItemIterator it = upload.getItemIterator(request);
    	      while (it.hasNext()) {
    	        FileItemStream item = it.next();
    	        InputStream stream = item.openStream();
    	        
    	        if (typeFile == "xls") {    	        	
    	        	List<List<HSSFCell>> cellDataList = new ArrayList<List<HSSFCell>>();	
    	        	/*
    					* Create a new instance for HSSFWorkBook Class
    					*/
    						HSSFWorkbook workBook = new HSSFWorkbook(stream);
    						HSSFSheet hssfSheet = workBook.getSheetAt(0);
    						
    						/**
    						* Iterate the rows and cells of the spreadsheet
    						* to get all the datas.
    						*/
    							Iterator<Row> rowIterator = hssfSheet.rowIterator();
    							while (rowIterator.hasNext())
    							{
    								HSSFRow hssfRow = (HSSFRow) rowIterator.next();
    								Iterator<Cell> iterator = hssfRow.cellIterator();
    								List<HSSFCell> cellTempList = new ArrayList<HSSFCell>();
    								while (iterator.hasNext())
    								{
    										HSSFCell hssfCell = (HSSFCell) iterator.next();
    										cellTempList.add(hssfCell);
    								}
    								cellDataList.add(cellTempList);
    							}	
    							
    							JSONArray rows = new JSONArray();
    				    		  
    	    					for (int i = 0; i < cellDataList.size(); i++)
    	    					{
    	    						List<HSSFCell> cellTempList = cellDataList.get(i);
    	    						JSONObject cells = new JSONObject();
    	    						for (int j = 0; j < cellTempList.size(); j++)
    	    						{

    	    							HSSFCell hssfCell = (HSSFCell) cellTempList.get(j);
    	    							StringBuilder build = new StringBuilder();
    	    							build.append("C");
    	    							build.append(j);
    	    							cells.put(build.toString(), hssfCell.toString());	
    	    						}
    	    						rows.put(cells);
    	    					}
    	    					
    	    					return rows.toString(); 
    							
    	        } else if (typeFile == "xlsx"){
    	        	List<List<XSSFCell>> cellDataList = new ArrayList<List<XSSFCell>>();
    	        	/* Create a new instance for HSSFWorkBook Class
    				*/
    					XSSFWorkbook workBook = new XSSFWorkbook(stream);
    					XSSFSheet hssfSheet = workBook.getSheetAt(0);

    				/**
    				* Iterate the rows and cells of the spreadsheet
    				* to get all the datas.
    				*/
    					Iterator<Row> rowIterator = hssfSheet.rowIterator();
    					while (rowIterator.hasNext())
    					{
    						XSSFRow hssfRow = (XSSFRow) rowIterator.next();
    						Iterator<Cell> iterator = hssfRow.cellIterator();
    						List<XSSFCell> cellTempList = new ArrayList<XSSFCell>();
    						while (iterator.hasNext())
    						{
    							XSSFCell hssfCell = (XSSFCell) iterator.next();
    							cellTempList.add(hssfCell);
    						}
    						cellDataList.add(cellTempList);
    					}		  
    					
    					JSONArray rows = new JSONArray();
    		  
    					for (int i = 0; i < cellDataList.size(); i++)
    					{
    						List<XSSFCell> cellTempList = cellDataList.get(i);
    						JSONObject cells = new JSONObject();
    						for (int j = 0; j < cellTempList.size(); j++)
    						{

    							XSSFCell xssfCell = (XSSFCell) cellTempList.get(j);
    							cells.put(String.valueOf(j), xssfCell.toString());	
    						}
    						rows.put(cells);
    					}
    					return rows.toString();
        	        
    	        }   	       
				
    	         	        
    	      }
    	    } catch (Exception ex) {
    	     // throw new ServletException(ex);
    	    }
    	return "";
    }

   public String processFileName(String fileNameInput) {
     String fileNameOutput=null;
     fileNameOutput = fileNameInput.substring(
  fileNameInput.lastIndexOf("\\")+1,fileNameInput.length());
     return fileNameOutput;
   }
    
}