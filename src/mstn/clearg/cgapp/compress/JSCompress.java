package mstn.clearg.cgapp.compress;

import java.io.IOException;
//import java.io.StringReader;
//import java.io.StringWriter;

import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

//import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

public class JSCompress {

	public static String compress(String func) throws IOException {
		//StringReader js = new StringReader(func.toString());
		//JavaScriptCompressor compress = new JavaScriptCompressor(js, new YuiCompressorErrorReporter());
		//StringWriter out = new StringWriter();
		Options o = new Options();
		//compress.compress(out, o.lineBreakPos, o.munge, o.verbose, o.preserveAllSemiColons, o.disableOptimizations);
		return o.toString();
		
	}
	
	public static class YuiCompressorErrorReporter implements ErrorReporter {
		public void warning(String message, String sourceName, int line, String lineSource, int lineOffset) {
			if (line < 0) {
				//logger.log(Level.WARNING, message);
			} else {
				//logger.log(Level.WARNING, line + ':' + lineOffset + ':' + message);
			}
		}

		public void error(String message, String sourceName, int line, String lineSource, int lineOffset) {
			if (line < 0) {
				//logger.log(Level.SEVERE, message);
			} else {
				//logger.log(Level.SEVERE, line + ':' + lineOffset + ':' + message);
			}
		}

		public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
			error(message, sourceName, line, lineSource, lineOffset);
			return new EvaluatorException(message);
		}
	}

	public static class Options {
		//public String charset = "UTF-8";
		public int lineBreakPos = -1;
		public boolean munge = true;
		public boolean verbose = true;
		public boolean preserveAllSemiColons = false;
		public boolean disableOptimizations = false;
	}

}
