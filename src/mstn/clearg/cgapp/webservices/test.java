package mstn.clearg.cgapp.webservices;

import java.util.prefs.Preferences;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import mstn.clearg.security.policy.Policies;
import mstn.clearg.security.policy.Policy;


@Path("test")
public class test {
    @Context
    private UriInfo context;

    /**
     * Default constructor. 
     */
    public test() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Retrieves representation of an instance of test
     * @return an instance of String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
    	try {

    		StringBuilder data = new StringBuilder("");
			///return mstn.clearg.IO.Shell.execCommand("");
    		Preferences prefs = Preferences.userRoot().node("MSTNMEDIA/jsuero");
    		data.append(prefs.get("username","jsuero"));
    		
    		//prefs.put("username","jose suero");
    		prefs.remove("username");
    		
    		return data.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return e.toString();
		}
    	
    }

    /**
     * PUT method for updating or creating an instance of test
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    
    public String createPolicy(){
        try {
        	Policies policies = new Policies("CGERPTEST");
        	Policy pol1 = policies.addPolicy("POLICY 1","true");
        	pol1.addRule("CARTONE SCHEMA", "/CARTONE/SCHEMA");
        	pol1.addSchema("CARTONE");
        	
        	Policy pol2 = policies.addPolicy("POLICY 2","false");       	
        	pol2.addRule("CARTONE SCHEMA", "/CARTONE/SCHEMA");
        	pol2.addSubject("users","group");
        	return policies.getXML();
        	/*
        	List<Policy> policies = new ArrayList<Policy>();        	
        	
        	Policy pol1 = new Policy("POLICY 1");
        	pol1.addRule("CARTONE OBJECT TABLES", "/CARTONE/OBJECTS/TABLES");
        	pol1.addRule("CARTONE SCHEMA", "/CARTONE/SCHEMA");
        	pol1.addRule("CARTONE TABLES", "/CARTONE/TABLES/*");
        	pol1.addReferral("CARTONE");
        	pol1.addReferral("CGERPTEST");
        	
        	
        	//No referral
        	
        	Policy pol1 = new Policy("POLICY 2");
            pol1.setReferralPolicy("false");
                        
            //pol1.addRule("CARTONE OBJECT TABLES","/CARTONE/OBJECTS/TABLES").addAttribute("GET","allow"); 
            pol1.addRule("CARTONE SCHEMA","/CARTONE/SCHEMA").addAttribute("GET","allow");
            pol1.addSubject("users", "AMIdentitySubject","inclusive").addAttribute("Values", "id=users,ou=group,o=CGERPTEST,ou=services,dc=mstn,dc=com");
            pol1.getSubjects().setName("Subjects:13359117669762jY4sA0=");
            pol1.getSubjects().setDescripion("");
            //pol1.getSubjects().setIncludeType("inclusive");
            //pol1.addReferral("CGERPTEST");
            
            cgSecurity security = new cgSecurity("");
            security.login("", "", "");
            //policies.add(pol1);
            //return security.createPolicy("CGERPTEST", policies,"");
            return "";
            
            String policyStr = policy.createPolicy("CGERPTEST", policies);

            
            return result + policyStr;
            */
		} catch (Exception e) {
			return e.toString();
		}    	
    }

}