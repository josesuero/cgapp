package mstn.clearg.cgapp.cgSequence;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import javax.naming.NamingException;

import mstn.clearg.language.cgLanguage;

import org.json.JSONException;

public class cgSequenceList extends HashMap<String, cgSequence> {

	private static final long serialVersionUID = 1L;
	
	public void add(String schema, String sequence) throws UnsupportedEncodingException, SQLException, NamingException, JSONException{
		this.put(cgLanguage.concat(schema,sequence), new cgSequence(schema, sequence));
	}
	
	public cgSequence get(String schema, String sequence) throws UnsupportedEncodingException, SQLException, NamingException, JSONException{
		String key = cgLanguage.concat(schema, sequence);
		if (!this.containsKey(key)){
			this.add(schema, sequence);
		}
		return this.get(key);
	}
	
	public void resync() throws SQLException, NamingException, UnsupportedEncodingException, JSONException{
		Iterator<String> itr = keySet().iterator(); 
		while(itr.hasNext()){
			this.get(itr.next()).resync();
		}
	}
}
