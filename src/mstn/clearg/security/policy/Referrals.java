package mstn.clearg.security.policy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Referrals{

	private List<Rule> referral;
	private String name;
	
	
	public Referrals(){
		referral = new ArrayList<Rule>();
	}
	@XmlElement(name="Referral")
	public List<Rule> getReferal(){
		return this.referral;
	}
	
	public void setPolicy(List<Rule> referral){
		this.referral = referral;
	}
	
	@XmlAttribute
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	private String Description;
	
	@XmlAttribute
	public String getDescription(){
		return this.Description;
	}
	
	public void setDescription(String description){
		this.Description = description;
	}

}
