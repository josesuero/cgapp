package mstn.clearg.language;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mstn.clearg.cache.CGCache;
import mstn.clearg.cache.CGCache.CacheLevel;
import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.compress.JSCompress;
//import mstn.clearg.cgapp.cgSequence.cgSequenceList;
import mstn.clearg.cgapp.mail.cgMail;
import mstn.clearg.rest.RestClient;
import mstn.clearg.security.cgSecurity;
import mstn.clearg.cgdata.cgDataWrapper;
import mstn.clearg.collections.cgArgs;
import mstn.clearg.collections.cgQueryList;
import mstn.clearg.collections.cgQueryObj;

import org.mozilla.javascript.*;

//import com.sun.jersey.api.client.Client;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class cgLanguage {

	private String data;
	private String schema;
	private String token;
	private cgSecurity cgsecurity;
	// private cgSequenceList seqList;
	private cgDataWrapper dataWrapper;

	private CGCache cacheGlobal;

	// LANGUAGE

	public cgLanguage(String token, String schema, String data, cgSecurity security) throws cgAppException, NumberFormatException, JSONException {
		this.schema = schema;
		this.data = data;
		this.cgsecurity = security;
		this.token = token;
		// this.seqList = seqList;
		this.dataWrapper = new cgDataWrapper(this.schema, token, cgsecurity);
		cacheGlobal = new CGCache("", "", CacheLevel.Global);
	}

	public cgLanguage(String token, String schema, String data,
			cgSecurity security, cgDataWrapper dataWrapper)
			throws cgAppException {
		this.schema = schema;
		this.data = data;
		this.cgsecurity = security;
		this.token = token;
		// this.seqList = seqList;
		this.dataWrapper = dataWrapper;
	}

	private Object process(String procedure, String data, cgDataWrapper cgdata)
			throws UnsupportedEncodingException, JSONException, SQLException,
			NamingException, cgAppException, IllegalAccessException,
			InstantiationException, InvocationTargetException {

		try {
			// Context cx = Context.enter();
			Context cx = new cgContextFactory().enterContext();

			// cgLanguage cgappWrapper = new cgLanguage(token, schema,data,
			// cgsecurity, seqList);
			cgProcedure proc = getName(procedure);

			JSONArray procedureObj = (new cgDataWrapper(proc.getObjSchema(),token)).query(
					"select CODE from cg_procedures where NAME = ?",
					(new cgArgs()).add(proc.getName(), 3)).data();

			// dataWrapper = new cgDataWrapper(schema, token, cgsecurity);

			if (procedureObj.length() == 0) {
				throw new cgAppException(404, cgLanguage.concat("Procedure ",
						procedure, " not found"));
			}

			String script = procedureObj.getJSONObject(0).getString("CODE");

			Scriptable scope = cx.initStandardObjects();

			Object dataobj = "";
			if (!(data == null) && (data.length() > 0)) {
				if (data.startsWith("{") || data.startsWith("({")) {
					dataobj = new JSONObject(data);
				} else if (data.startsWith("[") || data.startsWith("([")) {
					dataobj = new JSONArray(data);
				} else {
					dataobj = data;
				}
			}

			// cgSecurity cgsecurity = new cgSecurity();

			// cgParams cgparams = new cgParams(schema, dataWrapper);
			int step = 0;

			ScriptableObject.putProperty(scope, "data",
					Context.javaToJS(dataobj, scope));
			ScriptableObject.putProperty(scope, "cgdata",
					Context.javaToJS(cgdata, scope));
			ScriptableObject.putProperty(scope, "cgapp",
					Context.javaToJS(this, scope));
			ScriptableObject.putProperty(scope, "step",
					Context.javaToJS(step, scope));
			// ScriptableObject.putProperty(scope,
			// "cgSequence",Context.javaToJS(this.seqList, scope));

			// ScriptableObject.putProperty(scope, "cgsecurity",
			// Context.javaToJS(cgsecurity, scope));
			// ScriptableObject.putProperty(scope, "cgparam",
			// Context.javaToJS(cgparams, scope));

			ScriptableObject.defineClass(scope, cgArgs.class, false, true);
			ScriptableObject.defineClass(scope, cgQueryObj.class);
			ScriptableObject.defineClass(scope, cgQueryList.class);

			Object result = cx.evaluateString(scope, script, procedure, 1, null);
			// Script script = cx.compileString(source, sourceName, 1, null);
			// Object result = script.exec(cx, scope);
			if (result instanceof NativeJavaObject) {
				return ((NativeJavaObject) result).unwrap();
			} else {
				return result;
			}
		} finally {
			if (Context.getCurrentContext() != null)
				Context.exit();
		}
	}

	public Object execprocedure(String procedure)
			throws UnsupportedEncodingException, IllegalAccessException,
			InstantiationException, InvocationTargetException, cgAppException,
			JSONException, SQLException, NamingException {
		return execprocedure(procedure, this.data, this.schema);
	}

	public Object execprocedure(String procedure, String data)
			throws UnsupportedEncodingException, IllegalAccessException,
			InstantiationException, InvocationTargetException, cgAppException,
			JSONException, SQLException, NamingException {
		return execprocedure(procedure, data, this.schema);
	}

	public Object execprocedure(String procedure, String data, String schema)
			throws cgAppException, UnsupportedEncodingException,
			IllegalAccessException, InstantiationException,
			InvocationTargetException, JSONException, SQLException,
			NamingException {
		if (token == null || token == "") {
			throw new cgAppException(401, "token cannot be null");
		} else if (procedure == null || procedure == "") {
			throw new cgAppException(403, "procedure cannot be null");
		} else if (cgsecurity.authorize("PROCEDURES", this.schema, procedure,
				"GET", token)) {
			cgDataWrapper cgdata = this.dataWrapper;
			if (!this.dataWrapper.getSchema().equals(schema)) {
				cgdata = new cgDataWrapper(schema, token, this.cgsecurity);
			}

			return process(procedure, data, cgdata);
		} else {
			throw new cgAppException(403, cgLanguage.concat(
					"Access denied: PROCEDURE ", procedure));
		}
	}

	public Object createSchema(String schema, String username, String name,
			String password, String email) throws IOException,
			InterruptedException {
		String command = concat("/data/apps/clearg/addDB ", " ", schema, " ",
				username, " ", name, " ", password, " ", email);
		final Process process = Runtime.getRuntime().exec(command);
		int returnCode = process.waitFor();
		// System.out.println("Return code = " + returnCode);

		return returnCode;
	}

	public void sendMail(String recipients[], String subject, String message,
			String from) throws MessagingException {
		cgMail.postMail(recipients, subject, message, from);
	}

	public cgProcedure getName(String name) {
		cgProcedure proc = new cgProcedure(name, this.schema);
		return proc;
	}

	public boolean authorize(String type, String name, String action)
			throws cgAppException, NumberFormatException, JSONException {
		return cgsecurity
				.authorize(type, this.schema, name, action, this.token);
	}

	public void error(int status, Object content) throws cgAppException {
		throw new cgAppException(status, content.toString());
	}

	public String getSchema() {
		return this.schema;
	}

	public String getToken() {
		return this.token;
	}

	public JSONObject newJSONObject() {
		return new JSONObject();
	}

	public JSONObject newJSONObject(String data) throws JSONException {
		return new JSONObject(data);
	}

	public JSONArray newJSONArray() {
		return new JSONArray();
	}

	public JSONArray newJSONArray(String data) throws JSONException {
		return new JSONArray(data);
	}

	public static String concat(String... strs) {
		StringBuilder strbuild = new StringBuilder();
		for (int i = 0; i < strs.length; i++) {
			if (strs[i] != null) {
				strbuild.append(strs[i]);
			}
		}
		return strbuild.toString();
	}

	public JSONObject getRecord(String schema, String table, String id)
			throws JSONException, SQLException, NamingException,
			cgAppException, IllegalAccessException, InstantiationException,
			InvocationTargetException, IOException {
		return getRecord(schema, table, id, false);
	}

	public JSONObject getRecord(String schema, String table, String id,
			boolean cached) throws JSONException, SQLException,
			NamingException, cgAppException, IllegalAccessException,
			InstantiationException, InvocationTargetException, IOException {
		if (!this.cgsecurity.authorize(table, schema, id, "GET", this.token)) {
			throw new cgAppException(403, concat("Access denied DATA: ", id,
					" for table ", table));
		}
		JSONObject record = null;
		String cachename = null;
		//TODO cache object not string
		if (cached) {
			String recordData = "";
			cachename = concat("CG_RECORD_", schema, "_", table, "_", id);
			recordData = (String)cacheGlobal.get(cachename);
			if (recordData != null && !recordData.isEmpty()) {
				record = new JSONObject(recordData);
			}
		}
		if (record == null) {
			cgDataWrapper tempData = new cgDataWrapper(schema, id);
			cgLanguage tempLanguage = new cgLanguage(token, schema, "",
					this.cgsecurity, tempData);
			record = (JSONObject) (tempLanguage.process(
					"CLEARG.CG_GET",
					concat("{table:'", table,
							"', fields:'*',filter:[{field:'_key',value:'", id,
							"'}]}"), tempData));
			if (cached) {
				cacheGlobal.put(cachename, record.toString());
			}
		}
		return record;
		/*
		 * if (procedureObj.length() > 0){ return procedureObj.getJSONObject(0);
		 * } else {
		 * 
		 * }
		 */
	}

	public static String objToXML(Object obj) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(obj.getClass());
		java.io.StringWriter writer = new StringWriter();
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE Policies PUBLIC \"-//OpenSSO Policy Administration DTD//EN\" \"jar://com/sun/identity/policy/policyAdmin.dtd\">");
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
		marshaller
				.setProperty(
						"com.sun.xml.internal.bind.xmlHeaders",
						"<!DOCTYPE Policies PUBLIC \"-//OpenSSO Policy Administration DTD//EN\" \"jar://com/sun/identity/policy/policyAdmin.dtd\">");
		marshaller.marshal(obj, writer);
		return writer.toString();

	}
/*
	public Policies policies() {
		return new Policies(this.schema);
	}

	public String createPolicy(Policies policies) throws NumberFormatException,
			JAXBException, cgAppException {
		return this.cgsecurity.createPolicy(this.schema, policies, this.token);
	}

	public String createPolicy(String schema, Policies policies)
			throws NumberFormatException, cgAppException, JAXBException {
		if (!cgsecurity.authorize("OBJECTS", "ROOT", "POLICY", "PUT",
				this.token)) {
			error(401, "Access Denied: create Policies");
		}
		return this.cgsecurity.createPolicy(schema, policies, this.token);
	}

	public JSONArray listIdentities(String type) throws cgAppException,
			JSONException {
		return cgsecurity.list(getSchema(), type, this.token);
	}

	public String addPolicyReferral(String realm, String policy, String referral)
			throws cgAppException, IOException, InterruptedException {
		return cgsecurity
				.addPolicyReferral(realm, policy, referral, this.token);
	}
*/
	public JSONObject currentUser() throws NumberFormatException,
			cgAppException, JSONException {
		JSONObject attributes = cgsecurity.currentUser(this.token);
		return attributes;
	}

	// CACHE

	public CGCache cacheUser() throws IOException, NumberFormatException,
			JSONException, cgAppException {
		return cache(CacheLevel.User);
	}

	public CGCache cacheGlobal() throws IOException, NumberFormatException,
			JSONException, cgAppException {
		return cacheGlobal;
	}
	
	public Object test(){
		return cacheGlobal.get("as");
	}

	public CGCache cacheSchema() throws IOException, NumberFormatException,
			JSONException, cgAppException {
		return cache(CacheLevel.Schema);
	}

	private CGCache cache(CacheLevel level) throws IOException,
			NumberFormatException, JSONException, cgAppException {
		String user = currentUser().getString("uid");
		return new CGCache(getSchema(), user, level);
	}

	// COMPRESS

	public String jscompress(String data) throws IOException {
		return JSCompress.compress(data);
	}
	
	//WEB REQUEST REST WEBSERVICE
	public String webRequest(JSONObject request) throws JSONException, NumberFormatException, IOException, cgAppException{
		Client client;
		Object clientCache = cacheGlobal.get("JerseyClient");
		if (clientCache == null){
			client = ClientBuilder.newClient();
			cacheGlobal.put("JerseyClient",client);
		} else {
			client = (Client)clientCache;
		}
		return RestClient.request(client, request).getString("content");
	}
}