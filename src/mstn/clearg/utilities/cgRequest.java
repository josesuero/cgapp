package mstn.clearg.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import mstn.clearg.language.cgLanguage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class cgRequest {
	private String token;
	private String uri;
	
	public cgRequest(String token, String uri){
		this.token = token;
		this.uri = uri;
	}
	
	public String senddata(String command, String data) {
		String result = "";
		try {
			String datastr = (data !=null) ? URLEncoder.encode(data, "UTF-8") : "";
			String tokenstr = (this.token != null) ? URLEncoder.encode(this.token, "UTF-8"):"";

			URL url = new URL(this.uri);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);

			OutputStreamWriter out = new OutputStreamWriter(connection
					.getOutputStream());
			out.write(cgLanguage.concat("data=" , datastr));
			out.write(cgLanguage.concat("&token=",tokenstr));
			out.write(cgLanguage.concat("&procedure=",command));		
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			String decodedString;

			while ((decodedString = in.readLine()) != null) {
				result = cgLanguage.concat(result,decodedString);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Object getJsonData(String command, String data) throws UnsupportedEncodingException {
		String result = senddata(command, data);
		Object json;
		try {
			if (result.startsWith("{")) {
				json = new JSONObject(result);
			} else {
				json = new JSONArray(result);
			}
		} catch (JSONException e) {
			json = null;
		}
		return json;
	}

	public String login(String data) {
		return senddata("login", data);
	}

	public String getScreen(String screenname) throws UnsupportedEncodingException,JSONException {
		   String name = screenname;
		   String schema = "";

		   if (name.indexOf('.') != -1){
		      String[] names = screenname.split("\\.");
		      name = names[1];
		      schema = cgLanguage.concat(names[0], ".");
		   }

		JSONObject screen = (JSONObject)getJsonData("CGDATA_", cgLanguage.concat("{datatype:\"procedure\", value:\"" , schema , "CGAP_SCREENS_GET\", data:\"[{value:'" , name , "',type:3}]\"}"));
		return screen.getJSONArray("data").getJSONObject(0).getString("CODE"); 
		//return java.net.URLDecoder.decode(, "UTF-8");
	}

	public Object toJSON(String data) throws JSONException{
		Object json;
		if (data.startsWith("{")) {
			json = new JSONObject(data);
		} else {
			json = new JSONArray(data);
		}
		return json;
	}

}