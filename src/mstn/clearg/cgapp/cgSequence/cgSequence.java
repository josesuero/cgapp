package mstn.clearg.cgapp.cgSequence;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;

import mstn.clearg.cgdata.cgSQL;
import mstn.clearg.collections.cgArgs;
import mstn.clearg.language.cgLanguage;

public class cgSequence {
    private int id = 0;
	private String name = "";
    private String schema = "";
    private long max = 0;
    private long  min = 0;
    private long interval = 1;
    private long cachesize = 20;
    private boolean cycle = true;
    private long currentValue = 0;
    
    private long currentcache;
	
	public cgSequence(){
		super();	
	}
	
	private Connection getDataSource(String type, String schema)
			throws NamingException, SQLException {

		DataSource clearg;

		InitialContext ctx = new InitialContext();
		String datasource = cgLanguage.concat("java:jdbc/",schema.toUpperCase() , type.toUpperCase());
		clearg = (DataSource) ctx.lookup(datasource);

		return clearg.getConnection();
	}	
	
	public cgSequence(String schema, String sequence) throws UnsupportedEncodingException, JSONException, SQLException, NamingException{
		Connection conn = getDataSource("READ", schema);
		cgArgs args = new cgArgs();
		args.add(sequence, 3);
		try{
			JSONObject seqdata = cgSQL.query(conn, "select * from cg_sequence where name = ?", args).data().getJSONObject(0);
			fill(schema,seqdata);
		} finally{
			conn.close();
		}
	}
	public cgSequence(String schema,JSONObject row) throws JSONException, SQLException, NamingException, UnsupportedEncodingException{
		super();
		fill(schema, row);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}

	public long getMin() {
		return min;
	}

	public void setMin(long min) {
		this.min = min;
	}

	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public long getCachesize() {
		return cachesize;
	}

	public void setCachesize(long cachesize) {
		this.cachesize = cachesize;
	}

	public boolean isCycle() {
		return cycle;
	}

	public void setCycle(boolean cycle) {
		this.cycle = cycle;
	}

	public long getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(long currentValue) {
		this.currentValue = currentValue;
	}

	public void fill(String schema, JSONObject row) throws JSONException, SQLException, NamingException, UnsupportedEncodingException{
		this.id = row.getInt("ID");
		this.name = row.getString("NAME");
		this.max = row.getLong("MAX");
		this.min = row.getLong("MIN");
		this.interval = row.getLong("SEQINTERVAL");
		this.cachesize = row.getLong("CACHE");
		this.cycle = row.getBoolean("CYCLE");
		this.currentValue = row.getLong("VALUE");
		this.schema = schema;
		getNextBatch();
	}
	
	public synchronized long getNext() throws Exception{
		if (max == 0 || ((currentValue + interval) <= max) && ((currentValue + interval) >= min)){
			currentValue += interval;
			currentcache--;
			if (this.cachesize == 0 || currentcache == 0){
				getNextBatch();
			}
			return currentValue;
		} else {
			throw new Exception("sequence exception");
		}
	}
	public long getCurrent(){
		return currentValue;
	}
	
	public void resync() throws SQLException, NamingException, UnsupportedEncodingException, JSONException{
		Connection conn = getDataSource("WRITE", schema);
		cgArgs args = new cgArgs();
		args.add(currentValue,2);
		args.add(id, 1);
		try{
			cgSQL.noquery(conn, "update cg_sequence set value = ? where id = ?", args);
		} finally{
			conn.close();
		}
		currentcache =  this.cachesize;
	}
	
	private void getNextBatch() throws SQLException, NamingException, UnsupportedEncodingException, JSONException{
		cgArgs args = new cgArgs();
		args.add(currentValue + cachesize,2);
		args.add(id, 1);
		Connection conn = getDataSource("WRITE", schema);
		try{
			cgSQL.noquery(conn,"update cg_sequence set value = ? where id =? ", args);
		} finally{
			conn.close();
		}
		currentcache =  this.cachesize;
	}
}