package mstn.clearg.IO;

import java.io.IOException;
import java.io.InputStreamReader;

import mstn.clearg.language.cgLanguage;

public class Shell {
	public Shell(){
	}

	public static String execCommand(String command) throws IOException, InterruptedException{
		Process addDB = Runtime.getRuntime().exec("/Users/josesuero/Software/OpenDJ-2.4.5/bin/status");
		int exitValue = addDB.waitFor();
		if (exitValue == 0){
			return addDB.getOutputStream().toString();
		} else {
			InputStreamReader read = new InputStreamReader(addDB.getInputStream());
			
			return cgLanguage.concat("Error: ",read.toString());
		}
	}

}
