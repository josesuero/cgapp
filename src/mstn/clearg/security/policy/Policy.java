package mstn.clearg.security.policy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement

@XmlType(propOrder={"active","referralPolicy","lastmodifieddate","creationdate","lastmodifiedby" ,"createdby","name","rule", "subjects", "referrals"})
public class Policy{

	private String name;
	private String createdby;
	private String lastmodifiedby;
	private String creationdate;
	private String lastmodifieddate;
	private String referralPolicy;
	private String active;
	private String version;
	private Referrals referrals;
	private Subjects subjects;
	private String schema; 
	
	public Policy(){
		this.setName("");
        this.setCreatedby("id=amadmin,ou=user,dc=mstn,dc=com");
        this.setLastmodifiedby("id=amadmin,ou=user,dc=mstn,dc=com");
        this.setCreationdate(String.valueOf(System.currentTimeMillis()));
        this.setLastmodifieddate(String.valueOf(System.currentTimeMillis()));
        this.setReferralPolicy("true");
        this.setActive("true");
        //this.setVersion("1.0");
		rules = new ArrayList<Rule>();
	}
	
	public Policy(String name){
		this();
		this.setName(name);
	}
	
	public Policy(String name,String schema){
		this(name);
		this.schema = schema;
	}
	
	@XmlAttribute
	public String getName(){
		return this.name;
	}
	public void setName(String policyName){
		this.name = policyName;
	}
	@XmlAttribute
	public String getCreatedby(){
		return this.createdby;
	}
	public void setCreatedby(String createdBy){
		this.createdby = createdBy;
	}
	
	@XmlAttribute
	public String getLastmodifiedby(){
		return this.lastmodifiedby;
	}
	
	public void setLastmodifiedby(String Lastmodifiedby){
		this.lastmodifiedby = Lastmodifiedby;
	}
	
	@XmlAttribute
	public String getCreationdate(){
		return this.creationdate;
	}
	
	public void setCreationdate(String Creationdate){
		this.creationdate = Creationdate;
	}
	
	@XmlAttribute
	public String getLastmodifieddate(){
		return lastmodifieddate;
	}
	
	public void setLastmodifieddate(String lastmodifieddate){
		this.lastmodifieddate = lastmodifieddate;
	}
	
	@XmlAttribute
	public String getReferralPolicy(){
		return this.referralPolicy;
	}
	
	public void setReferralPolicy(String referalPolicy){
		this.referralPolicy = referalPolicy;
	}
	
	@XmlAttribute
	public String getActive(){
		return this.active;
	}
	
	public void setActive(String active){
		this.active = active;
	}
	
	private List<Rule> rules;
	
	@XmlElement(name="Rule")
	public List<Rule> getRule(){
		return rules;
	}
	
	public void setRule(List<Rule> rules){
		this.rules = rules;
	}
	
	@XmlElement(name="Referrals")
	public Referrals getReferrals(){
		return this.referrals;
	}
	
	public void setReferrals(Referrals referrals){
		this.referrals = referrals;
	}

	public Rule addRule(String name, String resource){
		Rule rule = new Rule(name,resource);
		this.rules.add(rule);
		return rule;
	}
	
	public void addSchema(String name){
        if (this.referrals == null){
        	this.referrals = new Referrals();
        }
		this.referrals.setDescription("");
        this.referrals.setName("Referrals:1335908551179cwqIFmU=");
        Rule referral = new Rule();
        referral.setName(name.toUpperCase());
        referral.setType("SubOrgReferral");
        StringBuilder value = new StringBuilder();
        value.append("o=");
        value.append(name);
        value.append(",ou=services,dc=mstn,dc=com");
        referral.addAttribute("Values", value.toString());
        this.referrals.getReferal().add(referral);
	}

	@XmlElement(name="Subjects")
	public Subjects getSubjects() {
		return subjects;
	}

	public void setSubjects(Subjects subjects) {
		this.subjects = subjects;
	}
	
	public Rule addSubject(String name, String subjectType){
        if (this.subjects == null){
        	this.subjects = new Subjects();
        }
        return this.subjects.addSubject(name, subjectType,this.schema);
	}
	@XmlAttribute
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
