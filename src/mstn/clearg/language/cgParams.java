package mstn.clearg.language;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import javax.naming.NamingException;

import org.json.JSONArray;
import org.json.JSONException;

import mstn.clearg.cgdata.cgDataWrapper;
import mstn.clearg.collections.cgArgs;


public class cgParams {
	JSONArray params;
	public cgParams(String schema, cgDataWrapper cgdata){
		try {
			this.params = cgdata.ExecProcedureDataSet("CG_PARAMS_GETALL", new cgArgs()).data();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Object get(String name) throws JSONException{
		return params.getJSONObject(params.find("NAME", name)).get(name);
	}
	
	public String getString(String name) throws JSONException{
		return params.getJSONObject(params.find("NAME", name)).getString(name);
	}
	
	public Object getInt(String name) throws JSONException{
		return params.getJSONObject(params.find("NAME", name)).getInt(name);
	}
	public Object getLong(String name) throws JSONException{
		return params.getJSONObject(params.find("NAME", name)).getLong(name);
	}
	public Object getJSONObject(String name) throws JSONException{
		return params.getJSONObject(params.find("NAME", name)).getJSONObject(name);
	}
	public Object getJSONArray(String name) throws JSONException{
		return params.getJSONObject(params.find("NAME", name)).getJSONArray(name);
	}
}
