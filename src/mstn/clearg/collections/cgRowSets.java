package mstn.clearg.collections;


import java.sql.SQLException;

import org.json.JSONArray;

public class cgRowSets extends JSONArray{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public cgRowSets() throws SQLException {
		super();
	}
	
	public String toJSONString() {
		return this.toString();
	}

}
