package mstn.clearg.cgapp.webservices;

import javax.annotation.PostConstruct;
import javax.servlet.ServletConfig;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.cgAppResult;
import mstn.clearg.security.cgSecurity;

@Path("authorize")
public class authorize {
    @Context
    private UriInfo context;
    private cgSecurity cgsecurity;

    @Context ServletConfig sc;
    /**
     * Default constructor. 
     */
    public authorize() {

    }
    
    @PostConstruct
    public void init(){
    	this.cgsecurity = new cgSecurity();
    }


    /**
     * Retrieves representation of an instance of valid
     * @return an instance of String
     */
    @GET
    @Produces("application/json")
    public String getJson(@CookieParam("iPlanetDirectoryPro") @QueryParam("token") String token, @QueryParam("type") String type, @QueryParam("schema") String schema, @QueryParam("name") String name, @QueryParam("action") String action) {
    	cgAppResult result = new cgAppResult("valid");
    	try {
			result.setContent(this.cgsecurity.authorize(type, schema, name, action, token));
			result.setStatus(200);
		} catch (NumberFormatException e) {
			result.setStatus(500);
		} catch (cgAppException e) {
			if (e.getStatus() == 401){
				result.setContent(false);
			} else {
				result.setStatus(500);
			}
			result.setContent(e.getMessage());
		} catch (Exception e){
			result.setStatus(500);
			result.setContent(e.toString());
		}
    	return result.toString();
    }
}