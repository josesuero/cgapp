package mstn.clearg.collections;

import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.annotations.*;

public class cgQueryObj extends ScriptableObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 838078424169692929L;
	private String sqlstr;
	private cgArgs args;
	private boolean returnId;
	
	public cgQueryObj(){
		this.sqlstr = "";
		this.args = new cgArgs();
		this.returnId = false;
	}
	

	public cgQueryObj(String sqlstr, cgArgs args, Boolean returnId){
		if (returnId.equals(null)){
			returnId = false;
		}
		this.sqlstr = sqlstr;
		this.args = args;
		this.returnId = returnId;
	}
	
	@JSSetter("sqlstr")
	public void setsqlstr(String sqlstr) {
		this.sqlstr = sqlstr;
	}
	@JSGetter
	public String getsqlstr() {
		return sqlstr;
	}
	@JSSetter("args")
	public void setargs(cgArgs args) {
		this.args = args;
	}
	@JSGetter("args")
	public cgArgs getargs() {
		return args;
	}

	@JSSetter("ReturnId")
	public void setReturnId(boolean returnId) {
		this.returnId = returnId;
	}

	@JSGetter("ReturnId")
	public boolean isReturnId() {
		return returnId;
	}

	@Override
	public String getClassName() {
		return "cgQueryObj";
	}
}

