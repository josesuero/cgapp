package mstn.clearg.cgapp.webservices;

import javax.annotation.PostConstruct;
import javax.servlet.ServletConfig;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.cgAppResult;
import mstn.clearg.security.cgSecurity;

@Path("valid")
public class valid {
    @Context
    private UriInfo context;
    private cgSecurity cgsecurity;

    @Context ServletConfig sc;
    /**
     * Default constructor. 
     */
    public valid() {

    }
    
    @PostConstruct
    public void init(){
    	this.cgsecurity = new cgSecurity();
    }


    /**
     * Retrieves representation of an instance of valid
     * @return an instance of String
     */
    @GET
    @Produces("application/json")
    public String getJson(@QueryParam("token") String token, @QueryParam("callback") String callback) {
    	cgAppResult result = new cgAppResult("valid");
    	try {
			result.setContent(this.cgsecurity.isTokenValid(token));
			result.setStatus(200);
		} catch (NumberFormatException e) {
			result.setStatus(500);
		} catch (cgAppException e) {
			if (e.getStatus() == 401){
				result.setContent(false);
			} else {
				result.setStatus(500);
			}
			result.setContent(e.getMessage());
		} catch (Exception e){
			result.setStatus(500);
			result.setContent(e.toString());
		}
    	return result.toString();
    }

    /**
     * PUT method for updating or creating an instance of valid
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public String postJson(@FormParam("token") String token, @FormParam("callback") String callback) {
        return getJson(token, callback);
    }    
}