package mstn.clearg.cgdata;

import mstn.clearg.collections.*;
import mstn.clearg.language.cgLanguage;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import javax.naming.NamingException;

import org.json.JSONException;

public class cgSQL {

	/**
	 * Sends a Select Query to the Database server
	 * 
	 * @param schema
	 *            database schema [not working]
	 * @param sqlstr
	 *            SQL Query
	 * @return Returns a RowSet object with query results
	 * @throws SQLException
	 * @throws JSONException 
	 * @throws NamingException 
	 * @throws UnsupportedEncodingException 
	 */
	public static cgRowSet query(Connection conn, String sqlstr, cgArgs params) throws SQLException, NamingException, UnsupportedEncodingException, JSONException{
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(sqlstr);
			stmt = fillParams(stmt, params.getList());

			ResultSet rs = null;
			cgRowSet rset = new cgRowSet();
			rs = stmt.executeQuery();
			rset.populate(rs);
			rs.close();
			return rset;
		} finally {
			stmt.close();
		}
	}

	public static ArrayList<Integer> noquerybatch(Connection conn, cgQueryList queryList) throws SQLException, NamingException {
		ArrayList<Integer> id = new ArrayList<Integer>();
		LinkedList<cgQueryObj> list = queryList.getList();

		PreparedStatement stmt = null;
		
		try {
			conn.setAutoCommit(false);

			Iterator<cgQueryObj> iterator = list.iterator(); 
			while (iterator.hasNext()){
				cgQueryObj query = iterator.next();
				if (query.isReturnId()){
					stmt = conn.prepareStatement(query.getsqlstr(),Statement.RETURN_GENERATED_KEYS);
				} else {
					stmt = conn.prepareStatement(query.getsqlstr());
				}
				fillParams(stmt, query.getargs());
				stmt.executeUpdate();
				stmt.close();
			}
			
			stmt = conn.prepareStatement("select @id");
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()){
				id.add(rs.getInt(1));
			}
			rs.close();
			stmt.close();
			conn.commit();
			return id;
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		} catch (Exception e){
			conn.rollback();
			throw new SQLException(e.getMessage());
		} finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	/**
	 * Sends an insert, update, delete statement to the database
	 * 
	 * @param sqlstr
	 *            SQL Statement
	 * @return true for successful queries, false for failure
	 * @throws SQLException
	 * @throws NamingException 
	 * @throws JSONException 
	 * @throws UnsupportedEncodingException 
	 */
	
	public static boolean noquery(Connection conn, String sqlstr, cgArgs params) throws SQLException, NamingException, UnsupportedEncodingException, JSONException{
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(sqlstr);
			stmt = fillParams(stmt, params.getList());
			stmt.executeUpdate();
			return true;
		} finally {
			stmt.close();
		}
	}

	/**
	 * Execute a Function That returns an oracle cursor
	 * 
	 * @param schema
	 *            schema of the procedure
	 * @param procedure
	 *            Procedure Name
	 * @param args
	 *            String array with procedure arguments
	 * @return returns a rowset object with the data
	 * @throws SQLException
	 * @throws NamingException
	 */
	public static cgRowSet ExecProcedureDataSet(Connection conn,String schema,
			String procedure, cgArgs args) throws SQLException,
			NamingException, UnsupportedEncodingException, JSONException {

		CallableStatement stmt = createCallStatement(schema, procedure, args, conn,true);

		// execute and retrieve the result set
		cgRowSet rset = new cgRowSet();

		try {
			stmt.execute();
			ResultSet rs = (ResultSet) stmt.getObject(1);
			rset.populate(rs);
			rs.close();
		} finally {
			conn.close();
			stmt.close();
		}

		return rset;
	}

	/**
	 * Execute a procedure that returns no data
	 * 
	 * @param schema
	 *            schema of the procedure
	 * @param procedure
	 *            Procedure name
	 * @param args
	 *            String array of arguments
	 * @return returns a boolean value of the procedure result
	 * @throws SQLException
	 * @throws NamingException
	 */
	public static Boolean ExecProcedureNoquery(Connection conn,String schema, String procedure,
			cgArgs args) throws SQLException, NamingException {

		// cgConnection clearg = getDataSource(schema);
		// Connection conn = clearg.getCon();

		CallableStatement stmt = null;
		try {
			stmt = createCallStatement(schema, procedure, args, conn, false);
			// execute and retrieve the result set
			stmt.execute();
		} finally {
			conn.close();
			stmt.close();
		}
		return true;
	}

	public static CallableStatement createCallStatement(String schema,
			String procedure, cgArgs arguments, Connection conn,
			boolean rtncursor) throws SQLException, NamingException {
		
		ArrayList<cgArgument> args  = arguments.getList();
		
		PreparedStatement stmt;
		String query;
		if (rtncursor) {
			query = "begin ? := ";
		} else {
			query = "begin ";
		}
		String objSchema;

		if (!procedure.contains(".")) {
			objSchema = schema.toUpperCase();
			procedure = procedure.toUpperCase();
		} else {
			objSchema = procedure.split("\\.")[0].toUpperCase();
			procedure = procedure.split("\\.")[1].toUpperCase();
		}
		
		query = cgLanguage.concat(query,objSchema , "." , procedure);
		if (!(args == null) && (args.size() > 0)) {
			query = cgLanguage.concat(query, "(");
			query = cgLanguage.concat(query , "?");
			for (int i = 1; i < args.size(); i++) {
				query = cgLanguage.concat(query,",?");
			}
			query = cgLanguage.concat(query,")");
		}
		query = cgLanguage.concat(query,"; end;");

		stmt = conn.prepareCall(query);
		
		int diff = 1;
		if (rtncursor) {
			//stmt.registerOutParameter(1, OracleTypes.CURSOR);
			((CallableStatement)stmt).registerOutParameter(1, 10);
			diff = 2;
		}

		// register the type of the out param - an Oracle specific type
		// stmt.registerOutParameter(1, OracleTypes.CURSOR);

		// TODO : Include JDBC jar to change -10 to oracletypes.CURSOR
		// stmt.registerOutParameter(1, -10);
		// stmt.registerOutParameter(1, OracleTypes.CURSOR);

		// set the in param
		// Types
		// 0 Void
		// 1 String
		// 2 Number
		

		return (CallableStatement) fillParams(stmt, args, diff);
	}
	
	private static PreparedStatement fillParams(PreparedStatement stmt, cgArgs args) throws SQLException{
		return fillParams(stmt, args,1);
	}
	
	private static PreparedStatement fillParams(PreparedStatement stmt, cgArgs args, int diff) throws SQLException{
		return fillParams(stmt, args.getList(),diff);
	}	
	
	private static PreparedStatement fillParams(PreparedStatement stmt, ArrayList<cgArgument> args) throws SQLException{
		return fillParams(stmt, args,1);
	}

	private static PreparedStatement fillParams(PreparedStatement stmt, ArrayList<cgArgument> args, int diff) throws SQLException{
		if (!(args == null) && (args.size() > 0)) {
			for (int i = 0; i < args.size(); i++) {
				int argcount = i + diff;

				switch (args.get(i).getType()) {
				case 1:
					// NUMBER
					double num = 0;
					if (!args.get(i).getValue().equalsIgnoreCase("")) {
						num = Double.valueOf(args.get(i).getValue());
					}
					stmt.setInt(argcount, (int) (num));
					break;
				case 2:
				case 5:
					// NUMBER
					double value = 0;
					if (!args.get(i).getValue().equals("")) {
						value = Double.valueOf(args.get(i).getValue());
					}
					stmt.setDouble(argcount, value);
					break;
				case 3:
				case 4:
				case 6:
				case 15:
					// STRING
					stmt.setString(argcount, args.get(i).getValue());
					break;
				case 7:
					stmt.setInt(
							argcount,
							(args.get(i).getValue().equals("true") || args
									.get(i).getValue().equals(1)) ? 1 : 0);
					break;
				// DATE
				case 99: //15 temporary number 
					// CLOB

					StringReader reader1 = new StringReader(args.get(i)
							.getValue());
					stmt.setCharacterStream(argcount, reader1, args.get(i)
							.getValue().length());

					// CLOB v_clob = CLOB.createTemporary(conn,
					// true,CLOB.DURATION_CALL);
					// v_clob.putChars(argcount,
					// args.get(i).getValue().toCharArray());
					// stmt.setClob(1, v_clob);
					break;
				}
			}
		}
		return stmt;
	}
/*
 	public static boolean procedureExists(Connection conn,String schema, String procedure)
			throws SQLException, JSONException, UnsupportedEncodingException, NamingException {
		cgArgs args = new cgArgs();
		args.add(schema, 3);
		args.add(procedure,3);
 		String sqlstr = "select count(*) from all_procedures where owner = ?  and OBJECT_NAME = ?";
		cgRowSet rs = query(conn,"SYSTEM", sqlstr,args);
		if (rs.getJSONArray("data").length() == 0) {
			return false;
		} else {
			return true;
		}
	}
*/
}