package mstn.clearg.cgapp.webservices;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.servlet.ServletConfig;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.json.JSONException;

//import com.sun.jersey.spi.resource.Singleton;

import mstn.clearg.cgapp.cgAppException;
import mstn.clearg.cgapp.cgAppResult;
import mstn.clearg.security.cgSecurity;

@Singleton
@Path("logout")
public class logout {
    @Context
    private UriInfo context;

    @Context ServletConfig sc; 
    /**
     * Default constructor. 
     */
    private cgSecurity cgsecurity;
    public logout() {

    }


    @PostConstruct
    public void init(){
    	this.cgsecurity = new cgSecurity();
    }
    /**
     * Retrieves representation of an instance of logout
     * @return an instance of String
     * @throws JSONException 
     */
    @GET
    @Produces("application/json")
    public String getJson(@QueryParam("token") String token, @QueryParam("callback") String callback,@CookieParam("iPlanetDirectoryPro") String cookietoken) throws JSONException {
    	cgAppResult result = new cgAppResult("logout");
    	if (token == null){
			token = cookietoken;
    	}

		try {
			result.setContent(cgsecurity.logout(token).toString());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (cgAppException e) {
    		result.setStatus(e.getStatus());
    		if (e.getStatus() != 401){
    			result.setContent(e.getMessage());
    		}

		}
        if (callback != null){
        	StringBuilder strbuild = new StringBuilder(callback);
        	strbuild.append("(");
        	strbuild.append(result);
        	strbuild.append(")");
			return strbuild.toString();
		} else {
			return result.toString();	
		}        
    }

    /**
     * PUT method for updating or creating an instance of logout
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public String postJson(@FormParam("token") String token,  @FormParam("callback") String callback,@CookieParam("iPlanetDirectoryPro") String cookietoken) throws JSONException {
        return getJson(token, callback,cookietoken);
    }        
}